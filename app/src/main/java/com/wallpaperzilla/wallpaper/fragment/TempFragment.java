package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


/**
 * Created by home on 14/07/18.
 */


public class TempFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;

    ImageView imgv_menu;
    TextView txt_title;

    @SuppressLint("ValidFragment")
    public TempFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_temp, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        imgv_menu = view.findViewById(R.id.imgv_menu);
        txt_title = view.findViewById(R.id.txt_title);
    }

    private void setData() {

    }

    private void setLitionar() {
        imgv_menu.setOnClickListener(this);
    }

    private void setColor() {
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_menu) {
            //MainActivity.getInstance().setDrawer();
        }
    }

}