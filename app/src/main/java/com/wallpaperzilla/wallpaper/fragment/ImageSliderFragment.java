package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.LoginActivity;
import com.wallpaperzilla.wallpaper.activity.WatchVideoAdsActivity;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pl.droidsonroids.gif.GifImageView;


/**
 * Created by home on 14/07/18.
 */


@SuppressLint("ValidFragment")
public class ImageSliderFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    CardView cardview;
    ImageData featuredImgData;
    ImageView imageView;
    WallpaperManager wallpaperManager;
    Bitmap bitmap;
    ProgressDialog mProgressDialog, pd;
    AdView mAdView;
    GifImageView imgv_gif;

    @SuppressLint("ValidFragment")
    public ImageSliderFragment(ImageData featuredImgData) {
        this.featuredImgData = featuredImgData;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_image_slider, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        wallpaperManager = WallpaperManager.getInstance(context);
    }


    private void setView(View view) {
        imageView = view.findViewById(R.id.imageView);
        cardview = view.findViewById(R.id.cardview);
        imgv_gif = view.findViewById(R.id.imgv_gif);
        /*mAdView = view.findViewById(R.id.adView);*/
        mAdView = new AdView(context);
        if (!appPrefrece.getAddFree()) {
            setAdMobBannerAds();
        }
    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        mAdView.setAdSize(AdSize.LARGE_BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        cardview.addView(mAdView);
    }


    private void setData() {
        if (featuredImgData.isContainAds()) {
            imageView.setVisibility(View.GONE);
            mAdView.setVisibility(View.VISIBLE);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        } else {
            imageView.setOnClickListener(this);
            mAdView.setVisibility(View.GONE);

            imageLoader.displayImage(featuredImgData.getMImage(), imageView, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    imgv_gif.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageBitmap(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

        }

        /*imageView.setDefaultImageResId(R.drawable.black_bg);
        imageView.setErrorImageResId(R.drawable.black_bg);
        imageView.setImageUrl(featuredImgData.getThumbBig());*/
    }

    private void setLitionar() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == imageView) {

            if (featuredImgData.getIsPremium().equalsIgnoreCase("1")) {
                if (featuredImgData.getIsDwnld().equalsIgnoreCase("1")) {
                    showDialogImg();
                } else {
                    if (appPrefrece.getLogin()) {
                        showRewardDialog();
                    } else {
                        Intent i = new Intent(context, LoginActivity.class);
                        i.putExtra("fromApp", true);
                        context.startActivity(i);
                        ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                    }
                }
            } else {
                showDialogImg();
            }

        }

    }

    private void showDialogImg() {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        dialog.setContentView(R.layout.dialog_full_img);
        final ImageView imageView = (ImageView) dialog.findViewById(R.id.imgv_img);
        final ImageView imgv_download = (ImageView) dialog.findViewById(R.id.imgv_download);
        final ImageView imgv_open_wall = (ImageView) dialog.findViewById(R.id.imgv_open_wall);
        final GifImageView imgv_gif=dialog.findViewById(R.id.imgv_gif);
        final LinearLayout ll_both = (LinearLayout) dialog.findViewById(R.id.ll_both);
        LinearLayout ll_lock = (LinearLayout) dialog.findViewById(R.id.ll_lock);
        LinearLayout ll_home = (LinearLayout) dialog.findViewById(R.id.ll_home);
        LinearLayout ll_main = (LinearLayout) dialog.findViewById(R.id.ll_main);

        imgv_gif.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);

        ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ll_lock.setVisibility(View.VISIBLE);
        } else {
            ll_lock.setVisibility(View.GONE);
        }

        imageLoader.loadImage(featuredImgData.getMImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                imgv_gif.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(loadedImage);
            }
        });

        final ImageView imgv_temp = (ImageView) dialog.findViewById(R.id.imgv_temp);

        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bitmap == null) {
                    imageLoader.displayImage(featuredImgData.getOImage(), imgv_temp, Utility.getImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            pd = Utility.showProgressDialog(context);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            Utility.dismissProgressDialog(pd);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            try {
                                bitmap = loadedImage;
                                try {
                                    wallpaperManager.setBitmap(bitmap);
                                    Toast.makeText(context, "Wallpaper set successfully!!", Toast.LENGTH_SHORT).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                ll_both.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Utility.dismissProgressDialog(pd);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            Utility.dismissProgressDialog(pd);
                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        }
                    });
                } else {
                    try {
                        wallpaperManager.setBitmap(bitmap);
                        Toast.makeText(context, "Wallpaper set successfully!!", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ll_both.setVisibility(View.GONE);
                }
            }
        });

        ll_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bitmap == null) {
                    imageLoader.displayImage(featuredImgData.getOImage(), imgv_temp, Utility.getImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            pd = Utility.showProgressDialog(context);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            Utility.dismissProgressDialog(pd);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            try {
                                bitmap = loadedImage;
                                try {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
                                        Toast.makeText(context, "Lock wallpaper set successfully!!", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                ll_both.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Utility.dismissProgressDialog(pd);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            Utility.dismissProgressDialog(pd);
                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        }
                    });
                } else {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
                            Toast.makeText(context, "Lock wallpaper set successfully!!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ll_both.setVisibility(View.GONE);
                }
            }
        });

        imgv_open_wall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_both.getVisibility() == View.VISIBLE) {
                    ll_both.setVisibility(View.GONE);
                } else {
                    ll_both.setVisibility(View.VISIBLE);
                }
            }
        });

        imgv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                featuredImgData.setIsDwnld("1");
                int total_like = Integer.parseInt(featuredImgData.getTotalDwnld()) + 1;
                featuredImgData.setTotalDwnld(total_like + "");
                imageDownload();
                dowenload();
            }
        });

        dialog.show();
    }

    private void showRewardDialog() {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_custom_reward);


        // set the custom dialog components - text, image and button
        LinearLayout ll_main = dialog.findViewById(R.id.ll_main);
        ll_main.getLayoutParams().height = context.getResources().getDimensionPixelOffset(R.dimen._110sdp);
        ll_main.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen._250sdp);
        //ll_main.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;


        TextView txt_user_coin = (TextView) dialog.findViewById(R.id.txt_user_coin);
        int user_coin = 0, image_coin = 0, diff_coin = 0;
        try {
            user_coin = Integer.parseInt(appPrefrece.getProfile().getString("user_score"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        txt_user_coin.setText(user_coin + "");
        image_coin = Integer.parseInt(featuredImgData.getPremiumScore());
        TextView txt_image_coin = (TextView) dialog.findViewById(R.id.txt_image_coin);
        txt_image_coin.setText(image_coin + "");

        TextView txt_more_coin = (TextView) dialog.findViewById(R.id.txt_more_coin);
        txt_more_coin.setVisibility(View.INVISIBLE);

        final TextView txt_watch_video = (TextView) dialog.findViewById(R.id.txt_watch_video);
        if (image_coin > user_coin) {
            diff_coin = image_coin - user_coin;
            txt_more_coin.setVisibility(View.VISIBLE);
            txt_more_coin.setText("You need more " + diff_coin + " coins for download image");
            txt_watch_video.setText("Watch Video");
        } else {
            txt_watch_video.setText("Download");
        }

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        txt_watch_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt_watch_video.getText().toString().contains("Download")) {
                    featuredImgData.setIsDwnld("1");
                    int total_like = Integer.parseInt(featuredImgData.getTotalDwnld()) + 1;
                    featuredImgData.setTotalDwnld(total_like + "");
                    imageDownload();
                    changeUserScore(featuredImgData.getPremiumScore());
                    dowenload();
                } else {
                    Utility.gotoNext(context, WatchVideoAdsActivity.class);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void imageDownload() {
        apiServer.imageDownload(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {

            }

            @Override
            public void onFailure(String error) {

            }
        }, featuredImgData.getImgId() + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId() + "");

    }

    private void changeUserScore(String score) {
        pd = Utility.showProgressDialog(context);
        apiServer.changeUserScore(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        appPrefrece.setProfile(object.getJSONObject("user"));
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, "0", score + "", appPrefrece.getUserId());
    }

    private void dowenload() {

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Downloading...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);

        // execute this when the downloader must be fired
        final DownloadTask downloadTask = new DownloadTask(context);
        downloadTask.execute(featuredImgData.getOImage());

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });


    }

    private void callMediaScanner(File file) {

        MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    public class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                File file = new File(Utility.getAppPath());
                if (!file.exists()) {
                    file.mkdirs();
                }
                output = new FileOutputStream(Utility.getAppPath() + "/" + Utility.getFileNameFromUrl(sUrl[0]));

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
                File f = new File(Utility.getAppPath() + "/" + Utility.getFileNameFromUrl(featuredImgData.getOImage()));
                callMediaScanner(f);
            }
        }
    }

}