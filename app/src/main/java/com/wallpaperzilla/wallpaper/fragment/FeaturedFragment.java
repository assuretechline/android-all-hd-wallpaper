package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.LiveActivity;
import com.wallpaperzilla.wallpaper.activity.TrendingActivity;
import com.wallpaperzilla.wallpaper.activity.ViewImageActivity;
import com.wallpaperzilla.wallpaper.adapter.FeaturedGridAdapter;
import com.wallpaperzilla.wallpaper.adapter.TrendingImageAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.model.TagData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.wallpaperzilla.wallpaper.widget.HeaderGridView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by home on 14/07/18.
 */


public class FeaturedFragment extends Fragment implements View.OnClickListener {
    public static ArrayList<ImageData> trendingImageData = new ArrayList<>();
    public static String trendingReturnIds = "";
    public static int trendingTotalCount = 0;


    public static ArrayList<ImageData> liveImageData = new ArrayList<>();
    public static String liveReturnIds = "";
    public static int liveTotalCount = 0;


    TextView txt_more, txt_live_more;
    View headerView;
    RecyclerView rv_trending, rv_live;
    String currentVersion = "";
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    HeaderGridView gv_img;
    FeaturedGridAdapter featuredGridAdapter;
    ArrayList<TagData> tagDatas = new ArrayList<>();
    TrendingImageAdapter trendingImageAdapter, liveImageAdapter;

    ImageView imgv_type;
    String returnIds = "", imageIds = "";
    int totalCount = 0;
    ProgressDialog pd;

    boolean isApiCall = true, oneCall = true;

    @SuppressLint("ValidFragment")
    public FeaturedFragment() {
        oneCall = true;
        trendingImageData.clear();
        trendingReturnIds = "";
        trendingTotalCount = 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_featured, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        gv_img = view.findViewById(R.id.gv_img);
        headerView = LayoutInflater.from(context).inflate(R.layout.header_view_trending, null, false);
        rv_trending = headerView.findViewById(R.id.rv_trending);
        txt_more = headerView.findViewById(R.id.txt_more);
        rv_live = headerView.findViewById(R.id.rv_live);
        txt_live_more = headerView.findViewById(R.id.txt_live_more);

        trendingImageAdapter = new TrendingImageAdapter(context, trendingImageData);
        rv_trending.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rv_trending.setAdapter(trendingImageAdapter);

        liveImageAdapter = new TrendingImageAdapter(context, liveImageData);
        rv_live.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rv_live.setAdapter(liveImageAdapter);

        imgv_type = view.findViewById(R.id.imgv_type);
    }

    private void setData() {
        imgv_type.setVisibility(View.GONE);
        featuredGridAdapter = new FeaturedGridAdapter(context, tagDatas);

        if (Utility.isNetworkAvailable(context)) {
            pd = Utility.showProgressDialog(context);
            getTrendingLive();
        } else {
            Utility.dismissProgressDialog(pd);
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    @SuppressLint("NewApi")
    private void setLitionar() {
        imgv_type.setOnClickListener(this);
        txt_more.setOnClickListener(this);
        txt_live_more.setOnClickListener(this);
        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(context, ViewImageActivity.class);
                i.putExtra("imageData", tagDatas.get(position - 3).getImageData());
                i.putExtra("totalCount", tagDatas.get(position - 3).getImageTotalCount());
                i.putExtra("tag_id", tagDatas.get(position - 3).getTagId());
                context.startActivity(i);
                ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            }
        });
        gv_img.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisibleRow = gv_img.getFirstVisiblePosition();
                int lastVisibleRow = gv_img.getLastVisiblePosition();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    if ((tagDatas.size() - 1) == i) {
                        if (isApiCall) {
                            isApiCall = false;
                            if (totalCount > (returnIds.split(",").length)) {
                                if (Utility.isNetworkAvailable(context)) {
                                    getHome();
                                } else {
                                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });
    }

    private void setColor() {
        imgv_type.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_type) {
            if (Utility.isNetworkAvailable(context)) {
                if (appPrefrece.getIsHorizontal().equals("0")) {
                    imgv_type.setImageResource(R.drawable.horizontal_img);
                    appPrefrece.setIsHorizontal("1");
                    returnIds = "";
                    imageIds = "";
                } else {
                    imgv_type.setImageResource(R.drawable.vertical_img);
                    appPrefrece.setIsHorizontal("0");
                    returnIds = "";
                    imageIds = "";
                }
                tagDatas.clear();
                getHome();
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        } else if (view == txt_more) {
            Utility.gotoNext(context, TrendingActivity.class);
        } else if (view == txt_live_more) {
            Utility.gotoNext(context, LiveActivity.class);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        trendingImageAdapter.notifyDataSetChanged();
        if (!tagDatas.isEmpty()) {
            if (HomeFragment.mPosition == 1) {
                try {
                    currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                    new GetVersionCode().execute();
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getHome() {

        apiServer.getHome(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = true;
                try {
                    if (object.getBoolean("success")) {
                        JSONArray userData = object.getJSONArray("userData");
                        if (userData.length() == 1) {
                            appPrefrece.setProfile(userData.getJSONObject(0));
                        }
                        totalCount = Integer.parseInt(object.getString("tag_total_count"));
                        appPrefrece.setFORCE_UPDATE(object.getBoolean("android_force_update"));
                        if (tagDatas.isEmpty()) {
                            try {
                                currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                                new GetVersionCode().execute();
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                        for (int i = 0; i < object.getJSONArray("tags").length(); i++) {
                            JSONObject temp = object.getJSONArray("tags").getJSONObject(i);
                            JSONObject image = temp.getJSONObject("image");

                            ImageData imageData = new ImageData(
                                    image.getString("uploded_by_user_id") + "",
                                    image.getString("o_image") + "",
                                    image.getString("total_like") + "",
                                    image.getString("is_active") + "",
                                    image.getString("is_dwnld") + "",
                                    image.getString("uploded_by_user_name") + "",
                                    image.getString("is_fav") + "",
                                    image.getString("uploded_by_user_photo") + "",
                                    image.getString("img_id") + "",
                                    image.getString("s_image") + "",
                                    image.getString("is_premium") + "",
                                    image.getString("is_like") + "",
                                    image.getString("m_image") + "",
                                    image.getString("premium_score") + "",
                                    image.getString("total_dwnld") + "",
                                    image.getString("total_fav") + "");


                            tagDatas.add(new TagData(imageData,
                                    temp.getString("image_total_count") + "",
                                    temp.getString("tag_id") + "",
                                    temp.getString("tag") + ""));

                        }
                        returnIds = object.getString("returnIds");
                        imageIds = object.getString("imageIds");
                        featuredGridAdapter.notifyDataSetChanged();
                        Utility.dismissProgressDialog(pd);

                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                    /*Log.e("Tag -> ", "oneCall -> " + oneCall);
                    Log.e("Tag -> ", "appPrefrece.getEndTimeForFree() -> " + appPrefrece.getEndTimeForFree());
                    Log.e("Tag -> ", "appPrefrece.getEndTimeForInterval() -> " + appPrefrece.getEndTimeForInterval());
                    Log.e("Tag -> ", "object.getString(\"servertime\") -> " + object.getString("servertime"));*/
                    if (oneCall) {
                        appPrefrece.setAdsImage(object.getString("android_ads_image"));
                        appPrefrece.setAdsLink(object.getString("android_ads_link"));
                        appPrefrece.setAdsImage2(object.getString("android_ads2_image"));
                        appPrefrece.setAdsLink2(object.getString("android_ads2_link"));
                        oneCall = false;
                        String pattern = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
                        Date one=dateFormat.parse(object.getString("servertime")), two, three=dateFormat.parse(object.getString("servertime"));
                        if (appPrefrece.getEndTimeForFree() != "")
                            one = dateFormat.parse(appPrefrece.getEndTimeForFree());
                        two = dateFormat.parse(object.getString("servertime"));
                        if (appPrefrece.getEndTimeForInterval() != "")
                            three = dateFormat.parse(appPrefrece.getEndTimeForInterval());
                        int i = one.compareTo(two);
                        if (i == -1) {
                            //one<two = -1
                            appPrefrece.setTimeForFree(0);
                            appPrefrece.setAddFree(false);
                        } else if (i == 1) {
                            //one>two = 1
                            printDifferenceFree(two, one);
                        } else {
                            appPrefrece.setTimeForFree(0);
                            appPrefrece.setAddFree(false);
                            //one==two= 0
                        }

                        int i1 = three.compareTo(two);
                        if (i1 == -1) {
                            //three<two = -1
                            appPrefrece.setTimeForInterval(0);
                            appPrefrece.setAddInterval(false);
                        } else if (i1 == 1) {
                            //three>two = 1
                            printDifferenceInterval(two, three);
                        } else {
                            appPrefrece.setTimeForInterval(0);
                            appPrefrece.setAddInterval(false);
                            //three==two= 0
                        }
                    }

                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                } catch (ParseException e) {
                    //Log.e("Tag -> ", e.getMessage());
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, appPrefrece.getIsHorizontal(), appPrefrece.getUserId(), returnIds, imageIds);
    }

    public void printDifferenceFree(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        if (appPrefrece.getAddFree()) {
            appPrefrece.setTimeForFree(different);
            GlobalAppConfiguration.startTimer();
        }
    }

    public void printDifferenceInterval(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        if (appPrefrece.getAddInterval()) {
            appPrefrece.setTimeForInterval(different);
            GlobalAppConfiguration.startTimerForInterval();
        }
    }

    private void getTrendingLive() {

        apiServer.getTrendingLive(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        trendingTotalCount = Integer.parseInt(object.getString("t_image_total_count"));
                        if (trendingReturnIds.equals("")) {
                            trendingReturnIds = object.getString("t_returnIds");
                        } else {
                            trendingReturnIds += "," + object.getString("t_returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("t_images").length(); i++) {
                            JSONObject image = object.getJSONArray("t_images").getJSONObject(i);

                            if (trendingImageData.size() % 10 == 0 && trendingImageData.size() != 0 && !appPrefrece.getAddFree()) {
                                ImageData imageData = new ImageData();
                                imageData.setContainAds(true);
                                imageData.setAdsLoded(false);
                                trendingImageData.add(imageData);
                            } else {
                                ImageData imageData = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                imageData.setContainAds(false);
                                trendingImageData.add(imageData);
                            }

                        }
                        trendingImageAdapter.notifyDataSetChanged();
                        /*gv_img.addHeaderView(headerView);
                        gv_img.setAdapter(featuredGridAdapter);*/

                        /*liveTotalCount = Integer.parseInt(object.getString("l_image_total_count"));
                        if (liveReturnIds.equals("")) {
                            liveReturnIds = object.getString("l_returnIds");
                        } else {
                            liveReturnIds += "," + object.getString("l_returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("l_images").length(); i++) {
                            JSONObject image = object.getJSONArray("l_images").getJSONObject(i);

                            if (liveImageData.size() % 10 == 0 && liveImageData.size() != 0 && !appPrefrece.getAddFree()) {
                                ImageData imageData = new ImageData();
                                imageData.setContainAds(true);
                                imageData.setAdsLoded(false);
                                liveImageData.add(imageData);
                            } else {
                                ImageData imageData = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                imageData.setContainAds(false);
                                liveImageData.add(imageData);
                            }

                        }
                        liveImageAdapter.notifyDataSetChanged();*/
                        gv_img.addHeaderView(headerView);
                        gv_img.setAdapter(featuredGridAdapter);
                        getHome();
                    } else {
                        gv_img.setAdapter(featuredGridAdapter);
                        getHome();
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    gv_img.setAdapter(featuredGridAdapter);
                    getHome();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                gv_img.setAdapter(featuredGridAdapter);
                getHome();
            }
        }, appPrefrece.getIsHorizontal(), appPrefrece.getUserId());
    }

    private void getLiveImage() {

        apiServer.getLiveImage(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        liveTotalCount = Integer.parseInt(object.getString("image_total_count"));
                        if (liveReturnIds.equals("")) {
                            liveReturnIds = object.getString("returnIds");
                        } else {
                            liveReturnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {
                            JSONObject image = object.getJSONArray("images").getJSONObject(i);

                            if (liveImageData.size() % 10 == 0 && liveImageData.size() != 0 && !appPrefrece.getAddFree()) {
                                ImageData imageData = new ImageData();
                                imageData.setContainAds(true);
                                imageData.setAdsLoded(false);
                                liveImageData.add(imageData);
                            } else {
                                ImageData imageData = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                imageData.setContainAds(false);
                                liveImageData.add(imageData);
                            }

                        }
                        liveImageAdapter.notifyDataSetChanged();
                        gv_img.addHeaderView(headerView);
                        gv_img.setAdapter(featuredGridAdapter);
                        getHome();
                    } else {
                        gv_img.setAdapter(featuredGridAdapter);
                        getHome();
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    gv_img.setAdapter(featuredGridAdapter);
                    getHome();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                gv_img.setAdapter(featuredGridAdapter);
                getHome();
            }
        }, liveReturnIds, appPrefrece.getUserId());
    }

    private void updateConfirm() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog2.setTitle("Alert !");
        alertDialog2.setCancelable(false);
        // Setting Dialog Message
        alertDialog2.setMessage("A new update is available, please install it first.");

        if (!appPrefrece.getFORCE_UPDATE()) {
            alertDialog2.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
        }
        alertDialog2.setPositiveButton("Install",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en"));
                        startActivity(browserIntent);
                    }
                });

        alertDialog2.show();


    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            if (Utility.isNetworkAvailable(context)) {
                try {
                    newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                            .timeout(30000)
                            .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                            .referrer("http://www.google.com")
                            .get()
                            .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                            .first()
                            .ownText();
                    return newVersion;
                } catch (Exception e) {
                    return newVersion;
                }
            } else {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!currentVersion.equals(onlineVersion)) {
                    updateConfirm();
                }
            }

        }
    }
}