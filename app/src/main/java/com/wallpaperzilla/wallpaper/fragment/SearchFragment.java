package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.MainActivity;
import com.wallpaperzilla.wallpaper.activity.SearchByTagImageActivity;
import com.wallpaperzilla.wallpaper.activity.SearchByWordImageActivity;
import com.wallpaperzilla.wallpaper.adapter.PopUpWindowAdapter;
import com.wallpaperzilla.wallpaper.adapter.SearchGridAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.model.PopWindowData;
import com.wallpaperzilla.wallpaper.model.TagSearchData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by home on 14/07/18.
 */


public class SearchFragment extends Fragment implements View.OnClickListener {
    public static String returnIds = "";
    public static ArrayList<ImageData> imageData = new ArrayList<>();
    public static int totalCount = 0;
    public static String sortBy = "2";
    public static String tags = "";
    boolean isApiCall = true;

    LinearLayout ll_main;
    SearchGridAdapter searchGridAdapter;
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    GridView gv_img;

    ArrayList<TagSearchData> tagSearchData = new ArrayList<>();
    ImageView imgv_type, imgv_menu, imgv_down;
    TextView txt_sort;
    LinearLayout ll_sort;
    ProgressDialog pd;

    ChipCloud chipCloud;

    PopupWindow mypopupWindow;
    ArrayList<PopWindowData> popWindowData = new ArrayList<>();
    ListView lstv_popup_window;
    PopUpWindowAdapter popUpWindowAdapter;
    int lastPosition=0;

    @SuppressLint("ValidFragment")
    public SearchFragment() {
        imageData.clear();
        returnIds = "";
        totalCount = 0;
        tags = "";
        sortBy = "2";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_search, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        gv_img = view.findViewById(R.id.gv_img);
        imgv_type = view.findViewById(R.id.imgv_type);
        chipCloud = (ChipCloud) view.findViewById(R.id.chipCloud);
        imgv_menu = view.findViewById(R.id.imgv_menu);
        imgv_down = view.findViewById(R.id.imgv_down);
        txt_sort = view.findViewById(R.id.txt_sort);
        ll_sort = view.findViewById(R.id.ll_sort);
        ll_main = view.findViewById(R.id.ll_main);

    }

    private void setPopUpWindow() {

        popWindowData.add(new PopWindowData(context.getResources().getString(R.string.daily), false));
        popWindowData.add(new PopWindowData(context.getResources().getString(R.string.weekly), true));
        lastPosition=1;
        popWindowData.add(new PopWindowData(context.getResources().getString(R.string.monthly), false));
        popWindowData.add(new PopWindowData(context.getResources().getString(R.string.yearly), false));

        LayoutInflater inflater = (LayoutInflater)
                getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View myView = inflater.inflate(R.layout.popup_view, null);
        lstv_popup_window = myView.findViewById(R.id.lstv_menu);
        popUpWindowAdapter = new PopUpWindowAdapter(context, popWindowData);
        lstv_popup_window.setAdapter(popUpWindowAdapter);
        Utility.setCommonListViewHeightBasedOnChildren(lstv_popup_window);
        mypopupWindow = new PopupWindow(myView, context.getResources().getDimensionPixelOffset(R.dimen._100sdp), RelativeLayout.LayoutParams.WRAP_CONTENT, true);


        lstv_popup_window.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popWindowData.get(lastPosition).setSelected(false);
                lastPosition=position;
                popWindowData.get(position).setSelected(true);
                if (popWindowData.get(position).getTitle().equals(context.getResources().getString(R.string.daily))) {
                    sortBy = "1";
                } else if (popWindowData.get(position).getTitle().equals(context.getResources().getString(R.string.weekly))) {
                    sortBy = "2";
                } else if (popWindowData.get(position).getTitle().equals(context.getResources().getString(R.string.monthly))) {
                    sortBy = "3";
                } else if (popWindowData.get(position).getTitle().equals(context.getResources().getString(R.string.yearly))) {
                    sortBy = "4";
                } else {
                    sortBy = "2";
                }
                popUpWindowAdapter.notifyDataSetChanged();
                Utility.setCommonListViewHeightBasedOnChildren(lstv_popup_window);
                mypopupWindow.dismiss();

                if (Utility.isNetworkAvailable(context)) {
                    imageData.clear();
                    searchGridAdapter.notifyDataSetChanged();
                    getTagForSearch();
                } else {
                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                }
            }
        });
    }

    private void setData() {
        imgv_type.setVisibility(View.GONE);
        getTagForSearch();
        searchGridAdapter = new SearchGridAdapter(context, imageData);
        gv_img.setAdapter(searchGridAdapter);
        if(!appPrefrece.getAddFree()){
            setAdMobBannerAds();
        }
        setPopUpWindow();
    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setLitionar() {
        imgv_menu.setOnClickListener(this);
        txt_sort.setOnClickListener(this);
        ll_sort.setOnClickListener(this);
        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position % 10 == 0 && position != 0 && !appPrefrece.getAddFree()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    }
                    if (position % 20 == 0) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        }
                    }
                }else{
                    Intent i = new Intent(context, SearchByTagImageActivity.class);
                    i.putExtra("position", position);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                }

            }
        });
        gv_img.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisibleRow = gv_img.getFirstVisiblePosition();
                int lastVisibleRow = gv_img.getLastVisiblePosition();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    if ((imageData.size() - 1) == i) {
                        if (isApiCall) {
                            isApiCall = false;
                            if (totalCount > (returnIds.split(",").length)) {
                                if (Utility.isNetworkAvailable(context)) {
                                    imageSearchByTags();
                                } else {
                                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });

        chipCloud.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {
                tagSearchData.get(i).setSelected(true);
                imageData.clear();
                searchGridAdapter.notifyDataSetChanged();
                returnIds = "";
                totalCount = 0;
                tags = "";
                setTags();
            }

            @Override
            public void chipDeselected(int i) {
                tagSearchData.get(i).setSelected(false);
                imageData.clear();
                searchGridAdapter.notifyDataSetChanged();
                returnIds = "";
                totalCount = 0;
                tags = "";
                setTags();
            }
        });
    }

    private void setTags() {
        for (int i = 0; i < tagSearchData.size(); i++) {
            if (tagSearchData.get(i).isSelected()) {
                if (tags.equals("")) {
                    tags = tagSearchData.get(i).getTagId();
                } else {
                    tags += "," + tagSearchData.get(i).getTagId();
                }
            }
        }
        if (!tags.equals("")) {
            pd = Utility.showProgressDialog(context);
            imageSearchByTags();
        }
    }

    private void getTagForSearch() {

        if (sortBy.equals("1")) {
            txt_sort.setText(context.getResources().getString(R.string.daily));
        } else if (sortBy.equals("2")) {
            txt_sort.setText(context.getResources().getString(R.string.weekly));
        } else if (sortBy.equals("3")) {
            txt_sort.setText(context.getResources().getString(R.string.monthly));
        } else if (sortBy.equals("4")) {
            txt_sort.setText(context.getResources().getString(R.string.yearly));
        }
        pd = Utility.showProgressDialog(context);
        apiServer.getTagForSearch(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        JSONArray jsonArray = object.getJSONArray("tags");
                        String[] strArray = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            tagSearchData.add(new TagSearchData(jsonObject.getString("total") + "",
                                    jsonObject.getString("tag_id") + "",
                                    jsonObject.getString("created_at") + "",
                                    jsonObject.getString("tag") + "",
                                    false));
                            strArray[i] = jsonObject.getString("tag");
                        }
                        //String[] strArray = {"love", "romance", "city", "road", "work", "gym", "breackup", "sad", "army"};
                        chipCloud.removeAllViews();
                        chipCloud.addChips(strArray);
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, sortBy, appPrefrece.getIsHorizontal());
    }

    private void setColor() {
        imgv_type.setColorFilter(context.getResources().getColor(R.color.black_image));
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.black_image));
        imgv_down.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_menu) {
            MainActivity.getInstance().setDrawer();
        } else if (view == ll_sort) {
            mypopupWindow.showAsDropDown(ll_sort,-153,0);
            //popup.show();
        } else if (view == txt_sort) {
            mypopupWindow.showAsDropDown(ll_sort,-153,0);
            //popup.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (imageData.size() > 0) {
            searchGridAdapter.notifyDataSetChanged();
        }
    }

    private void imageSearchByTags() {
        apiServer.imageSearchByTags(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = true;
                try {
                    if (object.getBoolean("success")) {
                        totalCount = Integer.parseInt(object.getString("total_count"));
                        if (returnIds.equals("")) {
                            returnIds = object.getString("returnIds");
                        } else {
                            returnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {
                            JSONObject image = object.getJSONArray("images").getJSONObject(i);

                            if (imageData.size() % 10 == 0 && imageData.size() != 0  && !appPrefrece.getAddFree()) {
                                ImageData temp = new ImageData();
                                temp.setContainAds(true);
                                temp.setAdsLoded(false);
                                imageData.add(temp);
                            } else {
                                ImageData temp = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                temp.setContainAds(false);
                                imageData.add(temp);
                            }

                        }
                        searchGridAdapter.notifyDataSetChanged();
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, returnIds, appPrefrece.getIsHorizontal(), tags, appPrefrece.getUserId());
    }

}