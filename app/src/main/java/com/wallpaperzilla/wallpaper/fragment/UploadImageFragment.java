package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.AddImageActivity;
import com.wallpaperzilla.wallpaper.activity.MainActivity;
import com.wallpaperzilla.wallpaper.activity.SelectTagsActivity;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by home on 14/07/18.
 */


public class UploadImageFragment extends Fragment implements View.OnClickListener {
    public static ArrayList<ImageData> imagesPending = new ArrayList<>();
    public static ArrayList<ImageData> imagesApproved = new ArrayList<>();
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    ImageView imgv_menu;
    CircleImageView imgv_profile;
    FrameLayout fm_profile;
    TabLayout tablayout;
    ViewPager viewpager;
    ViewPagerAdapter viewPagerAdapter;
    UserApprovedPendingFragment approvedFragment;
    UserApprovedPendingFragment pendingFragmentt;
    ImageView imgv_add;
    ProgressDialog pd;
    String final_path = "";


    @SuppressLint("ValidFragment")
    public UploadImageFragment() {
        imagesPending.clear();
        imagesApproved.clear();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_upload_image, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        imgv_menu = view.findViewById(R.id.imgv_menu);
        viewpager = view.findViewById(R.id.viewpager);
        tablayout = view.findViewById(R.id.tablayout);
        imgv_profile = view.findViewById(R.id.imgv_profile);
        fm_profile = view.findViewById(R.id.fm_profile);
        imgv_add = view.findViewById(R.id.imgv_add);
    }

    private void setData() {
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewpager.setAdapter(viewPagerAdapter);
        tablayout.setupWithViewPager(viewpager);

        try {
            imageLoader.displayImage(appPrefrece.getProfile().getString("photo"), imgv_profile, Utility.getProfileImageOptions());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Utility.isNetworkAvailable(context)) {
            getMyUploadedImage();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    private void setLitionar() {
        imgv_menu.setOnClickListener(this);
        fm_profile.setOnClickListener(this);
        imgv_add.setOnClickListener(this);
    }

    private void updateUserPic() {
        pd = Utility.showProgressDialog(context);
        apiServer.updateUserPic(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {

                try {
                    if (object.getBoolean("success")) {
                        appPrefrece.setProfile(object.getJSONObject("user"));
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("messsage"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.dismissProgressDialog(pd);
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        }, new File(final_path), appPrefrece.getUserId());
    }

    private void setColor() {
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_menu) {
            MainActivity.getInstance().setDrawer();
        } else if (view == fm_profile) {
            if (Utility.isNetworkAvailable(context)) {
                fromCameraGallery();
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        } else if (view == imgv_add) {
            SelectTagsActivity.tagdIds="";
            Utility.gotoNext(context, AddImageActivity.class);
        }
    }

    private void fromCameraGallery() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog2.setTitle("Select from..");

       /* // Setting Dialog Message
        alertDialog2.setMessage("A new update is available, please install it first.");*/

        alertDialog2.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utility.openCamera(context);
                        dialog.cancel();
                    }
                });
        alertDialog2.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utility.openGallry(context);
                        dialog.cancel();
                    }
                });

        alertDialog2.show();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GlobalAppConfiguration.RESULT_TAKE) {
                final_path = "";
                File f = new File(String.valueOf((Utility.camera)));
                ImageLoader.getInstance().displayImage("file://" + f, imgv_profile, Utility.getImageOptions());
                if (f.exists()) {
                    final_path = Utility.camera + "";
                    updateUserPic();
                }
            } else if (requestCode == GlobalAppConfiguration.RESULT_GALLARY) {
                final_path = "";
                Uri uri = data.getData();
                String path = null;
                File f = null;
                try {
                    path = Utility.getRealPathFromURI(context, uri);
                    f = new File(path);
                    ImageLoader.getInstance().displayImage("file://" + f, imgv_profile, Utility.getImageOptions());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (path != null) {
                    final_path = path;
                    updateUserPic();
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }

    private void getMyUploadedImage() {
        pd = Utility.showProgressDialog(context);
        apiServer.getMyUploadedImage(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {

                try {
                    if (object.getBoolean("success")) {

                        imagesPending.clear();
                        imagesApproved.clear();

                        for (int i = 0; i < object.getJSONArray("imagesPending").length(); i++) {

                            if (imagesPending.size() % 10 == 0 && imagesPending.size() != 0 && !appPrefrece.getAddFree()) {
                                ImageData temp = new ImageData();
                                temp.setContainAds(true);
                                temp.setAdsLoded(false);
                                imagesPending.add(temp);
                            } else {
                                JSONObject image = object.getJSONArray("imagesPending").getJSONObject(i);
                                ImageData temp = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                temp.setContainAds(false);
                                imagesPending.add(temp);
                            }

                        }

                        for (int i = 0; i < object.getJSONArray("imagesApproved").length(); i++) {

                            if (imagesApproved.size() % 10 == 0 && imagesApproved.size() != 0 && !appPrefrece.getAddFree()) {
                                ImageData temp = new ImageData();
                                temp.setContainAds(true);
                                temp.setAdsLoded(false);
                                imagesApproved.add(temp);
                            } else {
                                JSONObject image = object.getJSONArray("imagesApproved").getJSONObject(i);
                                ImageData temp = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                temp.setContainAds(false);
                                imagesApproved.add(temp);
                            }

                        }
                        Utility.dismissProgressDialog(pd);
                        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
                        viewpager.setAdapter(viewPagerAdapter);
                        tablayout.setupWithViewPager(viewpager);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    //Log.e("Tag -> ", "JSONException -> " + e.toString());
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                //Log.e("Tag -> ", "error -> " + error);
                Utility.dismissProgressDialog(pd);
            }
        }, appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId());
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        String[] title = {"UPLOADED", "PENDING"};

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (approvedFragment == null) {
                    approvedFragment = new UserApprovedPendingFragment(true);
                }
                return approvedFragment;
            } else {
                if (pendingFragmentt == null) {
                    pendingFragmentt = new UserApprovedPendingFragment(false);
                }
                return pendingFragmentt;
            }
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    }
}