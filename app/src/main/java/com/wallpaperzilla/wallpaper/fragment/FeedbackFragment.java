package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.MainActivity;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by home on 14/07/18.
 */


public class FeedbackFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;

    ImageView imgv_menu;
    LinearLayout ll_main;
    EditText edt_email, edt_description;
    TextView txt_send;
    ProgressDialog pd;

    @SuppressLint("ValidFragment")
    public FeedbackFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_feedback, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        imgv_menu = view.findViewById(R.id.imgv_menu);
        ll_main = view.findViewById(R.id.ll_main);
        edt_email = view.findViewById(R.id.edt_email);
        edt_description = view.findViewById(R.id.edt_description);
        txt_send = view.findViewById(R.id.txt_send);
    }

    private void setData() {
        if (!appPrefrece.getAddFree()) {
            setAdMobBannerAds();
        }
    }
    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
    private void setLitionar() {
        imgv_menu.setOnClickListener(this);
        txt_send.setOnClickListener(this);
    }


    private void checkValidation() {
        String error = "";

        if (edt_email.getText().toString().trim().isEmpty()) {
            error = context.getString(R.string.enter_email);
        } else if (!isValidEmail(edt_email.getText().toString().trim())) {
            error = context.getString(R.string.enter_valid_email);
        } else if (edt_description.getText().toString().trim().isEmpty()) {
            error = context.getString(R.string.enter_feedback);
        }

        if (error.equals("")) {
            if (Utility.isNetworkAvailable(context)) {
                sendFeedBack();
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        } else {
            Utility.errDialog(error, context);
        }
    }

    private void sendFeedBack() {
        pd = Utility.showProgressDialog(context);
        apiServer.sendFeedBack(new APIResponse() {
                                   @Override
                                   public void onSuccess(JSONObject object) {
                                       try {
                                           if (object.getBoolean("success")) {
                                               edt_email.setText("");
                                               edt_description.setText("");
                                               edt_email.requestFocus();
                                               Utility.hideSoftKeyboard(ll_main,context);
                                               Utility.errDialog(object.getString("message"), context);
                                           } else {
                                               Utility.errDialog(object.getString("message"), context);
                                           }
                                       } catch (JSONException e) {
                                           e.printStackTrace();
                                           Utility.errDialog(getString(R.string.api_failure), context);
                                       }
                                       Utility.dismissProgressDialog(pd);
                                   }

                                   @Override
                                   public void onFailure(String error) {
                                       Utility.dismissProgressDialog(pd);
                                       Toast.makeText(context, getString(R.string.api_failure), Toast.LENGTH_SHORT).show();
                                   }
                               }, appPrefrece.getUserId(), edt_email.getText().toString(),edt_description.getText().toString());
    }

    private void setColor() {
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_menu) {
            MainActivity.getInstance().setDrawer();
        }else if(view==txt_send){
            checkValidation();
        }
    }

    private boolean isValidEmail(CharSequence email) {
        if (!TextUtils.isEmpty(email)) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
        return false;
    }

}