package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.DownloadImageActivity;
import com.wallpaperzilla.wallpaper.activity.MainActivity;
import com.wallpaperzilla.wallpaper.activity.SavedImageActivity;
import com.wallpaperzilla.wallpaper.adapter.SavedGridAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class SavedFragment extends Fragment implements View.OnClickListener {
    public static ArrayList<ImageData> imageData = new ArrayList<>();
    public static String returnIds = "";
    public static int totalCount = 0;
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    GridView gv_img;
    SavedGridAdapter savedGridAdapter;
    ImageView imgv_type;
    ProgressDialog pd;
    LinearLayout ll_main;
    TextView txt_no_data;

    boolean isApiCall = true;
    ImageView imgv_menu;

    @SuppressLint("ValidFragment")
    public SavedFragment() {
        imageData.clear();
        returnIds = "";
        totalCount = 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_saved, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        gv_img = view.findViewById(R.id.gv_img);
        imgv_type = view.findViewById(R.id.imgv_type);
        imgv_menu = view.findViewById(R.id.imgv_menu);
        ll_main = view.findViewById(R.id.ll_main);
        txt_no_data = view.findViewById(R.id.txt_no_data);
    }

    private void setData() {
        imgv_type.setVisibility(View.GONE);
        savedGridAdapter = new SavedGridAdapter(context, imageData);
        gv_img.setAdapter(savedGridAdapter);
        if (Utility.isNetworkAvailable(context)) {
            pd = Utility.showProgressDialog(context);
            getSavedImageList();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
        if(!appPrefrece.getAddFree()){
            setAdMobBannerAds();
        }
    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @SuppressLint("NewApi")
    private void setLitionar() {
        imgv_type.setOnClickListener(this);
        imgv_menu.setOnClickListener(this);
        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position % 10 == 0 && position != 0 && !appPrefrece.getAddFree()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    }
                    if (position % 20 == 0) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        }
                    }
                }else{
                    Intent i = new Intent(context, SavedImageActivity.class);
                    i.putExtra("position", position);
                    i.putExtra("totalCount", totalCount);
                    i.putExtra("returnIds", returnIds);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                }

            }
        });
        gv_img.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisibleRow = gv_img.getFirstVisiblePosition();
                int lastVisibleRow = gv_img.getLastVisiblePosition();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    if ((imageData.size() - 1) == i) {
                        if (isApiCall) {
                            isApiCall = false;
                            if (totalCount > (returnIds.split(",").length)) {
                                if (Utility.isNetworkAvailable(context)) {
                                    getSavedImageList();
                                } else {
                                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });
    }

    private void setColor() {
        imgv_type.setColorFilter(context.getResources().getColor(R.color.black_image));
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_type) {
            if (Utility.isNetworkAvailable(context)) {
                if (appPrefrece.getIsHorizontal().equals("0")) {
                    imgv_type.setImageResource(R.drawable.horizontal_img);
                    appPrefrece.setIsHorizontal("1");
                    returnIds = "";
                } else {
                    imgv_type.setImageResource(R.drawable.vertical_img);
                    appPrefrece.setIsHorizontal("0");
                    returnIds = "";
                }
                imageData.clear();
                getSavedImageList();
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        } else if (view == imgv_menu) {
            MainActivity.getInstance().setDrawer();
        }
    }

    private void getSavedImageList() {

        apiServer.getSavedImageList(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = true;
                try {
                    if (object.getBoolean("success")) {
                        txt_no_data.setVisibility(View.GONE);
                        totalCount = Integer.parseInt(object.getString("total_count"));
                        if (returnIds.equals("")) {
                            returnIds = object.getString("returnIds");
                        } else {
                            returnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {

                            if (imageData.size() % 10 == 0 && imageData.size() != 0 && !appPrefrece.getAddFree() ) {
                                ImageData temp = new ImageData();
                                temp.setContainAds(true);
                                temp.setAdsLoded(false);
                                imageData.add(temp);
                            } else {
                                JSONObject image = object.getJSONArray("images").getJSONObject(i);
                                ImageData temp = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                temp.setContainAds(false);
                                imageData.add(temp);
                            }

                        }


                        savedGridAdapter.notifyDataSetChanged();
                        Utility.dismissProgressDialog(pd);
                    } else {
                        txt_no_data.setText("Not Saved Yet!");
                        txt_no_data.setVisibility(View.VISIBLE);
                        Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, returnIds + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId());
    }

    @Override
    public void onResume() {
        super.onResume();
        savedGridAdapter.notifyDataSetChanged();
    }
}