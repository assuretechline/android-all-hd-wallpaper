package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.UploadImageActivity;
import com.wallpaperzilla.wallpaper.adapter.DownloadGridAdapter;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


/**
 * Created by home on 14/07/18.
 */


@SuppressLint("ValidFragment")
public class UserApprovedPendingFragment extends Fragment implements View.OnClickListener {

    boolean is_approved;
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    GridView gv_img;
    DownloadGridAdapter downloadGridAdapter;
    ProgressDialog pd;
    TextView txt_no_data;

    @SuppressLint("ValidFragment")
    public UserApprovedPendingFragment(boolean is_approved) {
        this.is_approved = is_approved;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_appreved_pending, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
//        setData();
        setLitionar();
        setColor();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        gv_img = view.findViewById(R.id.gv_img);
        txt_no_data = view.findViewById(R.id.txt_no_data);
        txt_no_data.setText("No image found.");
    }

    private void setData() {


        if (is_approved)
            downloadGridAdapter = new DownloadGridAdapter(context, UploadImageFragment.imagesApproved);
        else
            downloadGridAdapter = new DownloadGridAdapter(context, UploadImageFragment.imagesPending);
        gv_img.setAdapter(downloadGridAdapter);

        if (is_approved) {
            if (UploadImageFragment.imagesApproved.size() == 0) {
                gv_img.setVisibility(View.GONE);
                txt_no_data.setVisibility(View.VISIBLE);
            } else {
                gv_img.setVisibility(View.VISIBLE);
                txt_no_data.setVisibility(View.GONE);
            }
        } else {
            if (UploadImageFragment.imagesPending.size() == 0) {
                gv_img.setVisibility(View.GONE);
                txt_no_data.setVisibility(View.VISIBLE);
            } else {
                gv_img.setVisibility(View.VISIBLE);
                txt_no_data.setVisibility(View.GONE);
            }
        }

    }

    @SuppressLint("NewApi")
    private void setLitionar() {


        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (is_approved) {

                    if (position % 10 == 0 && position != 0 && !appPrefrece.getAddFree()) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                        }
                        if (position % 20 == 0) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                            }
                        }
                    } else {
                        Intent i = new Intent(context, UploadImageActivity.class);
                        i.putExtra("position", position);
                        context.startActivity(i);
                        ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                    }

                }

            }
        });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }
}