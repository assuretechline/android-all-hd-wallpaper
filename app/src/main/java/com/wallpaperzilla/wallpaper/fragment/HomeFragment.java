package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.LoginActivity;
import com.wallpaperzilla.wallpaper.activity.MainActivity;
import com.wallpaperzilla.wallpaper.activity.SearchByWordActivity;
import com.wallpaperzilla.wallpaper.activity.WatchVideoAdsActivity;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONException;


/**
 * Created by home on 14/07/18.
 */


public class HomeFragment extends Fragment implements View.OnClickListener {
    public static int mPosition = 0;
    public PremiumFragment premiumFragment;
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    ImageView imgv_menu, imgv_search;
    SmartTabLayout tablayout;
    ViewPager viewpager;
    ViewPagerAdapter viewPagerAdapter;
    FeaturedFragment featuredFragment;
    CategoryFragment categoryFragment;
    RandomFragment randomFragment;
    LinearLayout ll_coin;
    TextView txt_coin;

    @SuppressLint("ValidFragment")
    public HomeFragment() {
        mPosition = 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_home, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        imgv_menu = view.findViewById(R.id.imgv_menu);
        imgv_search = view.findViewById(R.id.imgv_search);
        viewpager = view.findViewById(R.id.viewpager);
        tablayout = view.findViewById(R.id.tablayout);
        ll_coin = view.findViewById(R.id.ll_coin);
        txt_coin = view.findViewById(R.id.txt_coin);
        ll_coin.setVisibility(View.GONE);
    }

    private void setData() {
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewpager.setAdapter(viewPagerAdapter);
        tablayout.setViewPager(viewpager);
        viewpager.setCurrentItem(1);
    }

    private void setLitionar() {
        imgv_menu.setOnClickListener(this);
        imgv_search.setOnClickListener(this);
        ll_coin.setOnClickListener(this);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                mPosition = i;
                if (i == 3) {
                    premiumFragment.onResume();
                    //if(appPrefrece.getLogin()){
                    ll_coin.setVisibility(View.VISIBLE);
                    imgv_search.setVisibility(View.GONE);
                    String coin = "0";
                    try {
                        coin = appPrefrece.getProfile().getString("user_score");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    txt_coin.setText(coin);
                    /*}else{
                        ll_coin.setVisibility(View.GONE);
                    }*/
                } else if (i == 1) {
                    ll_coin.setVisibility(View.GONE);
                    imgv_search.setVisibility(View.VISIBLE);
                } else if (i == 2) {
                    categoryFragment.onResume();
                    ll_coin.setVisibility(View.GONE);
                    imgv_search.setVisibility(View.GONE);
                }else if(i==0){
                    ll_coin.setVisibility(View.GONE);
                    imgv_search.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setColor() {
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.black_image));
        imgv_search.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPosition == 3) {
            //if(appPrefrece.getLogin()){
            ll_coin.setVisibility(View.VISIBLE);
            imgv_search.setVisibility(View.GONE);
            String coin = "0";
            try {
                coin = appPrefrece.getProfile().getString("user_score");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            txt_coin.setText(coin);
                    /*}else{
                        ll_coin.setVisibility(View.GONE);
                    }*/
        } else if (mPosition == 1) {
            ll_coin.setVisibility(View.GONE);
            imgv_search.setVisibility(View.VISIBLE);
        } else if(mPosition == 2){
            ll_coin.setVisibility(View.GONE);
            imgv_search.setVisibility(View.GONE);
        }else if (mPosition == 0) {
            ll_coin.setVisibility(View.GONE);
            imgv_search.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_menu) {
            MainActivity.getInstance().setDrawer();
        } else if (view == ll_coin) {
//            if (appPrefrece.getLogin()) {
                Utility.gotoNext(context, WatchVideoAdsActivity.class);
            /*} else {
                Intent i = new Intent(context, LoginActivity.class);
                i.putExtra("fromApp", true);
                context.startActivity(i);
                ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
            }*/
        } else if (view == imgv_search) {
            Utility.gotoNext(context, SearchByWordActivity.class);
        }
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        String[] title = {"RANDOM","FEATURED", "CATEGORIES", "PREMIUM"};

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (randomFragment == null) {
                    randomFragment = new RandomFragment();
                }
                return randomFragment;
            }else if (position == 1) {
                if (featuredFragment == null) {
                    featuredFragment = new FeaturedFragment();
                }
                return featuredFragment;
            } else if (position == 2) {
                if (categoryFragment == null) {
                    categoryFragment = new CategoryFragment();
                }
                return categoryFragment;
            } else {
                if (premiumFragment == null) {
                    premiumFragment = new PremiumFragment();
                }
                return premiumFragment;
            }
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    }

}