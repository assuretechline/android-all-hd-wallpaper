package com.wallpaperzilla.wallpaper.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.CategoryDetailActivity;
import com.wallpaperzilla.wallpaper.adapter.CategoryGridAdapter;
import com.wallpaperzilla.wallpaper.adapter.ColorCategoryAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.CategoryData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class CategoryFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    GridView gv_img;
    RecyclerView rv_color;
    ProgressDialog pd;
    LinearLayout ll_main;
    CategoryGridAdapter categoryGridAdapter;
    ArrayList<CategoryData> categoryDataList = new ArrayList<>();

    ColorCategoryAdapter colorCategoryAdapter;
    ArrayList<CategoryData> colorCategoryDataList = new ArrayList<>();
    AdView mAdView;

    @SuppressLint("ValidFragment")
    public CategoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_category, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setLitionar();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        gv_img = view.findViewById(R.id.gv_img);
        rv_color = view.findViewById(R.id.rv_color);
        ll_main = view.findViewById(R.id.ll_main);
    }

    private void setData() {
        categoryGridAdapter = new CategoryGridAdapter(context, categoryDataList);
        gv_img.setAdapter(categoryGridAdapter);
        if (!appPrefrece.getAddFree()) {
            setAdMobBannerAds();
        }
        if (Utility.isNetworkAvailable(context)) {
            getCategory();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    private void getCategory() {
       /* pd = Utility.showProgressDialog(context);
        Utility.dismissProgressDialog(pd);*/
        apiServer.getCategory(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        JSONArray categories = object.getJSONArray("categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject temp = categories.getJSONObject(i);
                            categoryDataList.add(new CategoryData(temp.getString("tag_id") + "",
                                    temp.getString("tag") + "",
                                    temp.getString("image_total_count") + "",
                                    temp.getString("tag_photo") + ""));
                        }


                        JSONArray colors = object.getJSONArray("colors");
                        for (int i = 0; i < colors.length(); i++) {
                            JSONObject temp = colors.getJSONObject(i);
                            colorCategoryDataList.add(new CategoryData(temp.getString("tag_id") + "",
                                    temp.getString("tag") + "",
                                    temp.getString("image_total_count") + "",
                                    temp.getString("tag_photo") + ""));
                        }
                        categoryGridAdapter.notifyDataSetChanged();
                        colorCategoryAdapter = new ColorCategoryAdapter(context, colorCategoryDataList);
                        rv_color.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        rv_color.setAdapter(colorCategoryAdapter);
//                        Utility.dismissProgressDialog(pd);


                    } else {
//                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Utility.dismissProgressDialog(pd);
                    Toast.makeText(context, getString(R.string.api_failure), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String error) {
//                Utility.dismissProgressDialog(pd);
                Toast.makeText(context, getString(R.string.api_failure), Toast.LENGTH_SHORT).show();
            }
        }, appPrefrece.getIsHorizontal());

    }

    private void getColor() {

    }

    private void setLitionar() {
        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(context, CategoryDetailActivity.class);
                i.putExtra("totalCount", categoryDataList.get(position).getImage_total_count());
                i.putExtra("tag_id", categoryDataList.get(position).getTag_id());
                i.putExtra("tag", Utility.titleWord(categoryDataList.get(position).getTag()));
                context.startActivity(i);
                ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            }
        });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
         mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mAdView!=null){
            if(appPrefrece.getAddFree()){
                mAdView.setVisibility(View.GONE);
            }else{
                mAdView.setVisibility(View.VISIBLE);
            }
        }
    }
}