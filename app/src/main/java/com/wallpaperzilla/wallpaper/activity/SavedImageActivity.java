package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.fragment.DownloadFragment;
import com.wallpaperzilla.wallpaper.fragment.SavedFragment;
import com.wallpaperzilla.wallpaper.fragment.SavedImageSliderFragment;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by home on 12/07/18.
 */

public class SavedImageActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;

    FrameLayout fl_bottom;
    TextView txt_close, txt_uploaded_by, txt_first_latter, txt_coin, txt_like, txt_download;
    ViewPager viewpager;
    MyCustomPagerAdapter myCustomPagerAdapter;

    ImageView imgv_blur, imgv_like, imgv_fav, imgv_download;
    LinearLayout ll_coin, ll_like, ll_dwnld, ll_admin_detail;
    CircleImageView imgv_user;
    WallpaperManager wallpaperManager;

    ProgressDialog pd;
    int mPosition = 0;
    ProgressDialog mProgressDialog;

    boolean isApiCall = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_image);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
        Utility.checkAndRequestPermissions(context);
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        wallpaperManager = WallpaperManager.getInstance(context);


        mPosition = getIntent().getIntExtra("position", 0);
    }

    private void setView() {
        viewpager = findViewById(R.id.viewpager);
        imgv_blur = findViewById(R.id.imgv_blur);
        imgv_like = findViewById(R.id.imgv_like);
        imgv_fav = findViewById(R.id.imgv_fav);
        imgv_download = findViewById(R.id.imgv_download);
        imgv_user = findViewById(R.id.imgv_user);
        txt_close = findViewById(R.id.txt_close_1);
        txt_uploaded_by = findViewById(R.id.txt_uploaded_by);
        txt_first_latter = findViewById(R.id.txt_first_latter);

        ll_coin = findViewById(R.id.ll_coin);
        ll_like = findViewById(R.id.ll_like);
        ll_dwnld = findViewById(R.id.ll_dwnld);
        txt_coin = findViewById(R.id.txt_coin);
        txt_like = findViewById(R.id.txt_like);
        txt_download = findViewById(R.id.txt_download);
        fl_bottom = findViewById(R.id.fl_bottom);
        ll_admin_detail = findViewById(R.id.ll_admin_detail);
    }

    private void setData() {


        myCustomPagerAdapter = new MyCustomPagerAdapter(getSupportFragmentManager(), SavedFragment.imageData);
        viewpager.setAdapter(myCustomPagerAdapter);
        viewpager.setCurrentItem(mPosition);
        setAllData(mPosition);
        /*if (Utility.isNetworkAvailable(context)) {
            pd = Utility.showProgressDialog(context);
            getImageByTag();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }*/

    }

    private void setLitionar() {
        selectImage(mPosition);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                mPosition = i;


                selectImage(i);
                setAllData(i);

                if (i > (SavedFragment.imageData.size() - 6)) {
                    if (SavedFragment.totalCount > SavedFragment.returnIds.split(",").length) {
                        if (Utility.isNetworkAvailable(context)) {
                            if (!isApiCall) {
                                isApiCall = true;
                                getSavedImageList();
                            }
                        } else {
                            Utility.errDialog(context.getResources().getString(R.string.network), context);
                        }
                    }
                }

                if (i != 0 && i % 5 == 0) {
                    if (i % 10 != 0) {
                        if (GlobalAppConfiguration.fbAds) {
                            GlobalAppConfiguration.fbAds = false;
                            Utility.showFaceBookIntestrialAd(context,appPrefrece);
                        } else {
                            GlobalAppConfiguration.fbAds = true;
                            Utility.showGoogleIntestrialAd(context,appPrefrece);
                        }
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        ll_dwnld.setOnClickListener(this);
        imgv_fav.setOnClickListener(this);
        ll_like.setOnClickListener(this);
        txt_close.setOnClickListener(this);
    }

    private void selectImage(int position) {
        String img = "";
        if (SavedFragment.imageData.get(mPosition).isContainAds()) {
            img = "drawable://" + R.drawable.icon;
        } else {
            img = SavedFragment.imageData.get(position).getSImage();
        }
        imageLoader.displayImage(img, imgv_blur, Utility.getImageOptions(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Bitmap blurred = blurRenderScript(loadedImage, 25);
                imgv_blur.setImageBitmap(blurred);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });
    }

    private void setAllData(int position) {

        if (SavedFragment.imageData.get(position).isContainAds()) {
            fl_bottom.setVisibility(View.GONE);
            ll_admin_detail.setVisibility(View.INVISIBLE);

        } else {
            fl_bottom.setVisibility(View.VISIBLE);
            ll_admin_detail.setVisibility(View.VISIBLE);
            if (SavedFragment.imageData.get(position).getIsLike().equals("1")) {
                imgv_like.setImageResource(R.drawable.like);
            } else {
                imgv_like.setImageResource(R.drawable.unlike);
            }
            if (SavedFragment.imageData.get(position).getIsFav().equals("1")) {
                imgv_fav.setImageResource(R.drawable.fav);
            } else {
                imgv_fav.setImageResource(R.drawable.unfav);
            }
            txt_uploaded_by.setText(SavedFragment.imageData.get(position).getUplodedByUserName());
            txt_like.setText(SavedFragment.imageData.get(position).getTotalLike());
            txt_download.setText(SavedFragment.imageData.get(position).getTotalDwnld());
            imageLoader.displayImage(SavedFragment.imageData.get(position).getUplodedByUserPhoto(), imgv_user, Utility.getProfileImageOptions());

            if (SavedFragment.imageData.get(position).getIsPremium().equals("0")) {
                ll_coin.setVisibility(View.GONE);
                txt_coin.setText(SavedFragment.imageData.get(position).getPremiumScore());
            } else {
                ll_coin.setVisibility(View.VISIBLE);
                txt_coin.setText(SavedFragment.imageData.get(position).getPremiumScore());
                if (SavedFragment.imageData.get(position).getIsDwnld().equals("1")) {
                    ll_coin.setAlpha((float) 0.5);
                } else {
                    ll_coin.setAlpha((float) 1.0);
                }
            }
        }
    }

    private void setColor() {
        //imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == txt_close) {
            Utility.gotoBack(context);
        } else if (view == ll_dwnld) {
            if (Utility.isNetworkAvailable(context)) {
                if (Utility.checkIfAlreadyStoragePermission(context)) {

                    if (SavedFragment.imageData.get(mPosition).getIsPremium().equals("1")) {
                        if (appPrefrece.getLogin()) {
                            if (SavedFragment.imageData.get(mPosition).getIsDwnld().equals("0")) {
                                showRewardDialog();
                            } else {
                                SavedFragment.imageData.get(mPosition).setIsDwnld("1");
                                int total_like = Integer.parseInt(SavedFragment.imageData.get(mPosition).getTotalDwnld()) + 1;
                                SavedFragment.imageData.get(mPosition).setTotalDwnld(total_like + "");
                                txt_download.setText(total_like + "");
                                dowenload();
                            }
                        } else {
                            Intent i = new Intent(context, LoginActivity.class);
                            i.putExtra("fromApp", true);
                            context.startActivity(i);
                            ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                        }
                    } else {
                        SavedFragment.imageData.get(mPosition).setIsDwnld("1");
                        int total_like = Integer.parseInt(SavedFragment.imageData.get(mPosition).getTotalDwnld()) + 1;
                        SavedFragment.imageData.get(mPosition).setTotalDwnld(total_like + "");
                        txt_download.setText(total_like + "");
                        imageDownload();
                        dowenload();
                    }

                } else {
                    Utility.checkAndRequestPermissions(context);
                }
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }

        } else if (view == imgv_fav) {
            if (Utility.isNetworkAvailable(context)) {
                if (appPrefrece.getLogin()) {
                    if (SavedFragment.imageData.get(mPosition).getIsFav().equals("1")) {
                        SavedFragment.imageData.get(mPosition).setIsFav("0");
                        imgv_fav.setImageResource(R.drawable.unfav);
                        imageFavDisFav();
                        SavedFragment.imageData.remove(mPosition);
                        if (SavedFragment.imageData.size() == 0) {
                            onBackPressed();
                        } else {
                            myCustomPagerAdapter.notifyDataSetChanged();
                            if(SavedFragment.imageData.get(viewpager.getCurrentItem()).isContainAds()){
                                SavedFragment.imageData.remove(viewpager.getCurrentItem());
                            }
                            myCustomPagerAdapter.notifyDataSetChanged();
                            mPosition=viewpager.getCurrentItem();
                            setAllData(mPosition);
                            selectImage(mPosition);
                        }

                    } else {
                        SavedFragment.imageData.get(mPosition).setIsFav("1");
                        imgv_fav.setImageResource(R.drawable.fav);
                    }

                } else {
                    Intent i = new Intent(context, LoginActivity.class);
                    i.putExtra("fromApp", true);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                }
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        } else if (view == ll_like) {
            if (Utility.isNetworkAvailable(context)) {
                if (appPrefrece.getLogin()) {
                    if (SavedFragment.imageData.get(mPosition).getIsLike().equals("1")) {
                        SavedFragment.imageData.get(mPosition).setIsLike("0");
                        imgv_like.setImageResource(R.drawable.unlike);
                        int total_like = Integer.parseInt(SavedFragment.imageData.get(mPosition).getTotalLike()) - 1;
                        SavedFragment.imageData.get(mPosition).setTotalLike(total_like + "");
                        txt_like.setText(total_like + "");

                    } else {
                        SavedFragment.imageData.get(mPosition).setIsLike("1");
                        imgv_like.setImageResource(R.drawable.like);
                        int total_like = Integer.parseInt(SavedFragment.imageData.get(mPosition).getTotalLike()) + 1;
                        SavedFragment.imageData.get(mPosition).setTotalLike(total_like + "");
                        txt_like.setText(total_like + "");
                    }
                    imageLikeDislike();
                } else {
                    Intent i = new Intent(context, LoginActivity.class);
                    i.putExtra("fromApp", true);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                }
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        }
    }

    private void showRewardDialog() {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_custom_reward);


        // set the custom dialog components - text, image and button
        LinearLayout ll_main = dialog.findViewById(R.id.ll_main);
        ll_main.getLayoutParams().height = context.getResources().getDimensionPixelOffset(R.dimen._110sdp);
        ll_main.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen._250sdp);
        //ll_main.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;


        TextView txt_user_coin = (TextView) dialog.findViewById(R.id.txt_user_coin);
        int user_coin = 0, image_coin = 0, diff_coin = 0;
        try {
            user_coin = Integer.parseInt(appPrefrece.getProfile().getString("user_score"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        txt_user_coin.setText(user_coin + "");
        image_coin = Integer.parseInt(SavedFragment.imageData.get(mPosition).getPremiumScore());
        TextView txt_image_coin = (TextView) dialog.findViewById(R.id.txt_image_coin);
        txt_image_coin.setText(image_coin + "");

        TextView txt_more_coin = (TextView) dialog.findViewById(R.id.txt_more_coin);
        txt_more_coin.setVisibility(View.INVISIBLE);

        final TextView txt_watch_video = (TextView) dialog.findViewById(R.id.txt_watch_video);
        if (image_coin > user_coin) {
            diff_coin = image_coin - user_coin;
            txt_more_coin.setVisibility(View.VISIBLE);
            txt_more_coin.setText("You need more " + diff_coin + " coins for download image");
            txt_watch_video.setText("Watch Video");
        } else {
            txt_watch_video.setText("Download");
        }

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        txt_watch_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt_watch_video.getText().toString().contains("Download")) {
                    SavedFragment.imageData.get(mPosition).setIsDwnld("1");
                    int total_like = Integer.parseInt(SavedFragment.imageData.get(mPosition).getTotalDwnld()) + 1;
                    SavedFragment.imageData.get(mPosition).setTotalDwnld(total_like + "");
                    txt_download.setText(total_like + "");
                    imageDownload();
                    changeUserScore(SavedFragment.imageData.get(mPosition).getPremiumScore());
                    dowenload();
                } else {
                    Utility.gotoNext(context, WatchVideoAdsActivity.class);
                }
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }

    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
            Utility.w=smallBitmap.getWidth();
            Utility.h=smallBitmap.getHeight();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap bitmap = Bitmap.createBitmap(Utility.w, Utility.h,
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    private void getSavedImageList() {
        apiServer.getSavedImageList(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = false;
                try {
                    if (object.getBoolean("success")) {
                        SavedFragment.totalCount = Integer.parseInt(object.getString("total_count"));
                        if (SavedFragment.returnIds.equals("")) {
                            SavedFragment.returnIds = object.getString("returnIds");
                        } else {
                            SavedFragment.returnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {
                            JSONObject image = object.getJSONArray("images").getJSONObject(i);

                            if (SavedFragment.imageData.size() % 10 == 0 && !appPrefrece.getAddFree()) {
                                ImageData imageData = new ImageData();
                                imageData.setContainAds(true);
                                imageData.setAdsLoded(false);
                                SavedFragment.imageData.add(imageData);
                            } else {
                                ImageData imageData = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                imageData.setContainAds(false);
                                SavedFragment.imageData.add(imageData);
                            }

                        }
                        myCustomPagerAdapter.notifyDataSetChanged();
                        //Utility.dismissProgressDialog(pd);
                    } else {
                        //Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    //Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                // Utility.dismissProgressDialog(pd);
            }
        }, SavedFragment.returnIds + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId());
    }

    private void imageLikeDislike() {
        apiServer.imageLikeDislike(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {

            }

            @Override
            public void onFailure(String error) {

            }
        }, SavedFragment.imageData.get(mPosition).getImgId() + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId() + "");
    }

    private void imageFavDisFav() {
        apiServer.imageFavDisFav(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {

            }

            @Override
            public void onFailure(String error) {

            }
        }, SavedFragment.imageData.get(mPosition).getImgId() + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId() + "");

    }

    private void imageDownload() {
        apiServer.imageDownload(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                setAllData(mPosition);
            }

            @Override
            public void onFailure(String error) {

            }
        }, SavedFragment.imageData.get(mPosition).getImgId() + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId() + "");

    }

    private void changeUserScore(String score) {
        pd = Utility.showProgressDialog(context);
        apiServer.changeUserScore(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        appPrefrece.setProfile(object.getJSONObject("user"));
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, "0", score + "", appPrefrece.getUserId());
    }

    private void dowenload() {

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Downloading...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);

        // execute this when the downloader must be fired
        final DownloadTask downloadTask = new DownloadTask(context);
        downloadTask.execute(SavedFragment.imageData.get(mPosition).getOImage());

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });


    }

    private void callMediaScanner(File file) {

        MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    public class MyCustomPagerAdapter extends FragmentStatePagerAdapter {

        ArrayList<ImageData> featuredImgDataArrayList;

        public MyCustomPagerAdapter(FragmentManager fm, ArrayList<ImageData> featuredImgDataArrayList) {
            super(fm);
            this.featuredImgDataArrayList = featuredImgDataArrayList;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new SavedImageSliderFragment(featuredImgDataArrayList.get(i));
            return fragment;
        }

        @Override
        public int getCount() {
            return featuredImgDataArrayList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return (position + 1) + "/" + featuredImgDataArrayList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }

    public class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                File file = new File(Utility.getAppPath());
                if (!file.exists()) {
                    file.mkdirs();
                }
                output = new FileOutputStream(Utility.getAppPath() + "/" + Utility.getFileNameFromUrl(sUrl[0]));

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
                File f = new File(Utility.getAppPath() + "/" + Utility.getFileNameFromUrl(SavedFragment.imageData.get(mPosition).getOImage()));
                callMediaScanner(f);
            }
        }
    }

}
