package com.wallpaperzilla.wallpaper.activity;

import android.support.v7.app.AppCompatActivity;


public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
