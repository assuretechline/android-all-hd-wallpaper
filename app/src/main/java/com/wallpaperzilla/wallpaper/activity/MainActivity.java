package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mindinventory.midrawer.MIDrawerView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.wallpaperzilla.wallpaper.BuildConfig;
import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.adapter.MenuAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.fragment.ContactUsFragment;
import com.wallpaperzilla.wallpaper.fragment.DownloadFragment;
import com.wallpaperzilla.wallpaper.fragment.FeedbackFragment;
import com.wallpaperzilla.wallpaper.fragment.HomeFragment;
import com.wallpaperzilla.wallpaper.fragment.SavedFragment;
import com.wallpaperzilla.wallpaper.fragment.SearchFragment;
import com.wallpaperzilla.wallpaper.fragment.ShareFragment;
import com.wallpaperzilla.wallpaper.fragment.UploadImageFragment;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.MenuData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    public static MainActivity mainActivity;
    Context context;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageView imgv_logo;
    CircleImageView cimgv_logo;
    MenuAdapter menuAdapter;
    MIDrawerView drawerLayout;
    NavigationView navigationView;
    FrameLayout fl_main;
    ListView lstview;
    String frg = "";
    TextView txt_made, txt_welcome;
    int singleClick = 0;
    ProgressDialog pd;
    DisplayImageOptions optionProfile;

    public HomeFragment homeFragment;
    OSPermissionSubscriptionState status;

    public static MainActivity getInstance() {
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        setView();
        if (GlobalAppConfiguration.Main_Url.equals("")) {
            setFirebaseData();
        } else {
            setData();
            setLitionar();
            setColor();
            Utility.checkAndRequestPermissions(context);
        }
    }

    private void initialize() {
        context = this;
        mainActivity = this;
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);

        optionProfile = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.icon)
                .showImageOnLoading(R.drawable.icon)
                .showImageOnFail(R.drawable.icon)
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        /*PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.wallpaperzilla.wallpaper", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }*/

    }

    private void setView() {

        fl_main = findViewById(R.id.fl_main);
        drawerLayout = findViewById(R.id.drawer_lauout_list);
        navigationView = findViewById(R.id.nvViewList);
        lstview = findViewById(R.id.lstview);
        imgv_logo = findViewById(R.id.imgv_logo);
        cimgv_logo = findViewById(R.id.cimgv_logo);
        txt_made = findViewById(R.id.txt_made);
        txt_welcome = findViewById(R.id.txt_welcome);
    }

    private void setData() {

        status = OneSignal.getPermissionSubscriptionState();
        status.getPermissionStatus().getEnabled();

        drawerLayout.setScrimColor(Color.TRANSPARENT);

        drawerLayout.setDrawerElevation(0);

        if (Utility.isNetworkAvailable(context)) {
            deviceInfo();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
        txt_made.setText(context.getResources().getString(R.string.Madewith) + " " + BuildConfig.VERSION_NAME);

        menuAdapter = new MenuAdapter(context, GlobalAppConfiguration.menuDataList);
        lstview.setAdapter(menuAdapter);
        Utility.setCommonListViewHeightBasedOnChildren(lstview);

        if (homeFragment == null) {
            homeFragment = new HomeFragment();
        }
        setSelection(0);
        singleClick = 0;
        setFragmentMainTab(homeFragment, "HomeFragment");
    }

    private void setWhistes() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            txt_welcome.setText("Good Morning, Dear!");
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            txt_welcome.setText("Good Afternoon, Dear!");
        } else {
            txt_welcome.setText("Good Evening, Dear!");
        }
    }

    private void deviceInfo() {

        apiServer.getLocation(new APIResponse() {
            @Override
            public void onSuccess(JSONObject objectMain) {
                try {
                    if (objectMain.getString("status").equals("success")) {
                      //  SharedPreferences pref = context.getSharedPreferences(GlobalAppConfiguration.SHARED_PREF, 0);
                        String one_signal_id =status.getSubscriptionStatus().getUserId();

                        apiServer.logIn(new APIResponse() {
                                            @Override
                                            public void onSuccess(JSONObject object) {
                                                try {
                                                    if (object.getBoolean("success")) {

                                                    } else {

                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onFailure(String error) {
                                                Utility.dismissProgressDialog(pd);
                                            }
                                        },
                                appPrefrece.getIS_FB() + "",
                                appPrefrece.getSOCIAL_ID() + "",
                                appPrefrece.getPHONE() + "",
                                appPrefrece.getEMAIL() + "",
                                appPrefrece.getNAME() + "",
                                appPrefrece.getPHOTO() + "",
                                objectMain.getString("country")+"",
                                Utility.getDeviceId(context) + "",
                                objectMain.getString("timezone")+"",
                                objectMain.getString("lat")+"",
                                objectMain.getString("lon")+"",
                                objectMain.getString("city")+"",
                                objectMain.getString("regionName")+"",
                                one_signal_id + ""
                        );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                //Utility.dismissProgressDialog(pd);
            }
        });
    }

    private void setLitionar() {
        lstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                setDrawer();

                if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.Home))) {
                    setSelection(i);
                    if (singleClick != i) {
                        singleClick = i;
                        setFragment(homeFragment, "HomeFragment");
                    }
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.Search))) {

                    if (Utility.isNetworkAvailable(context)) {
                        setSelection(i);
                        if (singleClick != i) {
                            singleClick = i;
                            setFragment(new SearchFragment(), "SearchFragment");
                        }

                    } else {
                        Utility.errDialog(context.getResources().getString(R.string.network), context);
                    }
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.Saved))) {
                    if (appPrefrece.getLogin()) {
                        setSelection(i);
                        if (singleClick != i) {
                            singleClick = i;
                            setFragment(new SavedFragment(), "SavedFragment");
                        }
                    } else {
                        login();
//                        Utility.errDialog(context.getResources().getString(R.string.login_required), context);
                    }
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.Downloads))) {
                    if (appPrefrece.getLogin()) {

                        if (singleClick != i) {
                            singleClick = i;
                            setFragment(new DownloadFragment(), "DownloadFragment");
                        }
                        setSelection(i);
                    } else {
                        login();
//                        Utility.errDialog(context.getResources().getString(R.string.login_required), context);
                    }
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.upload_image))) {
                    if (appPrefrece.getLogin()) {

                        if (singleClick != i) {
                            singleClick = i;
                            setFragment(new UploadImageFragment(), "UploadImageFragment");
                        }
                        setSelection(i);
                    } else {
//                        Utility.errDialog(context.getResources().getString(R.string.login_required), context);
                        login();
                    }
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().contains(getString(R.string.earn_coins)) || GlobalAppConfiguration.menuDataList.get(i).getTitle().contains(getString(R.string.ad_free_trial))) {
                    //if (appPrefrece.getLogin()) {
                    Utility.gotoNext(context, WatchVideoAdsActivity.class);
                    /*} else {
//                        Utility.errDialog(context.getResources().getString(R.string.login_required), context);
                        login();
                    }*/
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.ContactUs))) {
                    if (singleClick != i) {
                        singleClick = i;
                        setFragment(new ContactUsFragment(), "ContactUsFragment");
                    }
                    setSelection(i);
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.Feedback))) {
                    if (singleClick != i) {
                        singleClick = i;
                        setFragment(new FeedbackFragment(), "FeedbackFragment");
                    }
                    setSelection(i);
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.ShareAndRateus))) {
                    if (singleClick != i) {
                        singleClick = i;
                        setFragment(new ShareFragment(), "ShareFragment");
                    }
                    setSelection(i);
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.logout))) {
                    logoutConfirm();
                } else if (GlobalAppConfiguration.menuDataList.get(i).getTitle().equalsIgnoreCase(getString(R.string.login))) {
                    login();
                }

            }
        });
    }

    private void login() {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("fromApp", true);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
    }

    private void logoutConfirm() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog2.setTitle("Alert !");

        // Setting Dialog Message
        alertDialog2.setMessage("Are you sure you want to logout?");

        alertDialog2.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                        try {
                            JSONObject jsonObject = appPrefrece.getProfile();
                            if (jsonObject.getString("facebook_id").equals("")) {
                                LoginManager.getInstance().logOut();
                            }
                            if (jsonObject.getString("google_id").equals("")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        appPrefrece.remove();
                        GlobalAppConfiguration.menuDataList.remove((GlobalAppConfiguration.menuDataList.size() - 1));
                        GlobalAppConfiguration.menuDataList.add(new MenuData(getString(R.string.login), false));
                        menuAdapter.notifyDataSetChanged();
                        recreate();
                    }
                });
        alertDialog2.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog2.show();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*private void logoutConfirm() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_custom_error);
        dialog.setCancelable(true);
        LinearLayout ll_main = dialog.findViewById(R.id.ll_main);
        ll_main.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen._240sdp);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView text = (TextView) dialog.findViewById(R.id.txt_message);
        text.setText(error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void setSelection(int position) {
        for (int i = 0; i < GlobalAppConfiguration.menuDataList.size(); i++) {
            GlobalAppConfiguration.menuDataList.get(i).setSelecetd(false);
        }
        GlobalAppConfiguration.menuDataList.get(position).setSelecetd(true);
        menuAdapter.notifyDataSetChanged();
    }

    public void setDrawer() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GlobalAppConfiguration.menuDataList.get(GlobalAppConfiguration.menuDataList.size() - 1).getTitle().equalsIgnoreCase(getString(R.string.login)) ||
                GlobalAppConfiguration.menuDataList.get(GlobalAppConfiguration.menuDataList.size() - 1).getTitle().equalsIgnoreCase(getString(R.string.logout))) {
            GlobalAppConfiguration.menuDataList.remove(GlobalAppConfiguration.menuDataList.size() - 1);
        }

        setWhistes();

        if (appPrefrece.getLogin()) {
            GlobalAppConfiguration.menuDataList.add(new MenuData(getString(R.string.logout), false));
            try {
                imageLoader.displayImage(appPrefrece.getProfile().getString("photo"), cimgv_logo, optionProfile);
                txt_welcome.setText("Welcome " + appPrefrece.getProfile().getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            imageLoader.displayImage("drawable://" + R.drawable.icon, cimgv_logo, optionProfile);
            GlobalAppConfiguration.menuDataList.add(new MenuData(getString(R.string.login), false));
        }
        menuAdapter.notifyDataSetChanged();
        Utility.setListViewHeightBasedOnItems(lstview);
    }

    @Override
    public void onBackPressed() {
        int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
        String tag = backEntry.getName();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);

        if (drawerLayout.isDrawerOpen(navigationView)) {
            setDrawer();
        } else {
            if (fragment.getClass().equals(HomeFragment.class)) {
                finish();
            } else {
                super.onBackPressed();
                setSelection(0);
                singleClick = 0;
                menuAdapter.notifyDataSetChanged();
                setHomeFragment(homeFragment, "HomeFragment");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void setFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out, R.anim.slide_left_in, R.anim.slide_left_out).replace(R.id.fl_main, fragment, tag).addToBackStack(tag).commit();
    }

    public void setHomeFragment(Fragment fragment, String tag) {

        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out, R.anim.slide_right_in, R.anim.slide_right_out).replace(R.id.fl_main, fragment, tag).addToBackStack(tag).commit();
    }

    public void setFragmentMainTab(Fragment fragment, String tag) {
        if (tag.equals(frg)) {
        } else {
            frg = tag;
            getSupportFragmentManager().beginTransaction().replace(fl_main.getId(), fragment, tag).addToBackStack(tag).commitAllowingStateLoss();
        }
    }

    private void setFirebaseData() {
        if (Utility.isNetworkAvailable(context)) {
            try {
                pd = Utility.showProgressDialog(context);
                FirebaseApp.initializeApp(context);
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        appPrefrece.setAPP_ID(dataSnapshot.child("android_go_appID").getValue().toString());
                        appPrefrece.setBANNER_ID(dataSnapshot.child("android_go_banner").getValue().toString());
                        appPrefrece.setREWARD_ID(dataSnapshot.child("android_go_reward").getValue().toString());
                        appPrefrece.setINTESTRIAL_ID(dataSnapshot.child("android_go_in").getValue().toString());
                        appPrefrece.setFB_INTESTRIAL_ID(dataSnapshot.child("android_fb_in").getValue().toString());

                     /*   appPrefrece.setAPP_ID(dataSnapshot.child("test_android_go_appID").getValue().toString());
                        appPrefrece.setBANNER_ID(dataSnapshot.child("test_android_go_banner").getValue().toString());
                        appPrefrece.setREWARD_ID(dataSnapshot.child("test_android_go_reward").getValue().toString());
                        appPrefrece.setINTESTRIAL_ID(dataSnapshot.child("test_android_go_in").getValue().toString());
                        appPrefrece.setFB_INTESTRIAL_ID(dataSnapshot.child("test_android_fb_in").getValue().toString());*/

                        GlobalAppConfiguration.Main_Url = dataSnapshot.child("main_url").getValue().toString();

                        Utility.dismissProgressDialog(pd);

                        setData();
                        setLitionar();
                        setColor();
                        Utility.checkAndRequestPermissions(context);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utility.dismissProgressDialog(pd);
                        networkTryAdain();
                    }
                });
            } catch (Exception e) {
                Utility.dismissProgressDialog(pd);
                networkTryAdain();
            }
        } else {
            networkTryAdain();
        }
    }


    private void networkTryAdain() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Alert !");
        alertDialog2.setCancelable(false);
        alertDialog2.setMessage(context.getResources().getString(R.string.network));
        alertDialog2.setPositiveButton("Try Again",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setFirebaseData();
                        dialog.cancel();
                    }
                });
        alertDialog2.show();
    }
}

