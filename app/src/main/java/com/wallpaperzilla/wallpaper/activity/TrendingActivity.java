package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.adapter.SearchGridAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.fragment.FeaturedFragment;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by home on 12/07/18.
 */

public class TrendingActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    ImageView imgv_back, imgv_type;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;

    ProgressDialog pd;
    GridView gv_img;
    boolean isApiCall = true;
    SearchGridAdapter searchGridAdapter;
    LinearLayout ll_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        gv_img = findViewById(R.id.gv_img);
        imgv_type = findViewById(R.id.imgv_type);
        ll_main = findViewById(R.id.ll_main);
    }

    private void setData() {
        imgv_type.setVisibility(View.GONE);
        searchGridAdapter = new SearchGridAdapter(context, FeaturedFragment.trendingImageData);
        gv_img.setAdapter(searchGridAdapter);
        if (!appPrefrece.getAddFree()) {
            setAdMobBannerAds();
        }
    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);

        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if(position % 10 == 0 && position != 0 && !appPrefrece.getAddFree()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    }
                    if (position % 20 == 0) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        }
                    }
                }else{
                    Intent i = new Intent(context, TrendingImageActivity.class);
                    i.putExtra("position", position);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                }

            }
        });


        gv_img.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisibleRow = gv_img.getFirstVisiblePosition();
                int lastVisibleRow = gv_img.getLastVisiblePosition();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    if ((FeaturedFragment.trendingImageData.size() - 1) == i) {
                        if (isApiCall) {
                            isApiCall = false;
                            if (FeaturedFragment.trendingTotalCount > (FeaturedFragment.trendingReturnIds.split(",").length)) {
                                if (Utility.isNetworkAvailable(context)) {
                                    getTrendingImage();
                                } else {
                                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (FeaturedFragment.trendingImageData.size() > 0) {
            searchGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }

    private void getTrendingImage() {

        apiServer.getTrendingImage(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = true;
                try {
                    if (object.getBoolean("success")) {
                        FeaturedFragment.trendingTotalCount = Integer.parseInt(object.getString("image_total_count"));
                        if (FeaturedFragment.trendingReturnIds.equals("")) {
                            FeaturedFragment.trendingReturnIds = object.getString("returnIds");
                        } else {
                            FeaturedFragment.trendingReturnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {
                            JSONObject image = object.getJSONArray("images").getJSONObject(i);

                            if (FeaturedFragment.trendingImageData.size() % 10 == 0 && !appPrefrece.getAddFree()) {
                                ImageData imageData = new ImageData();
                                imageData.setContainAds(true);
                                imageData.setAdsLoded(false);
                                FeaturedFragment.trendingImageData.add(imageData);
                            } else {
                                ImageData imageData = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                imageData.setContainAds(false);
                                FeaturedFragment.trendingImageData.add(imageData);
                            }

                        }
                        searchGridAdapter.notifyDataSetChanged();
                        //Utility.dismissProgressDialog(pd);
                    } else {
                        //Utility.dismissProgressDialog(pd);
                        Utility.errDialog(object.getString("message"), context);
                    }
                } catch (JSONException e) {
                    //Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                // Utility.dismissProgressDialog(pd);
            }
        }, FeaturedFragment.trendingReturnIds + "", appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId());
    }


}
