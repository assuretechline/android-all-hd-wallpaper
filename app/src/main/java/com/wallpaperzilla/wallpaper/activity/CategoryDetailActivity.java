package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.adapter.SearchGridAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 12/07/18.
 */

public class CategoryDetailActivity extends BaseActivity implements View.OnClickListener {
    public static String returnIds = "";
    public static ArrayList<ImageData> imageData = new ArrayList<>();
    public static int totalCount = 0;
    Context context;
    ImageView imgv_back, imgv_type;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ProgressDialog pd;
    GridView gv_img;
    boolean isApiCall = true;
    SearchGridAdapter searchGridAdapter;
    LinearLayout ll_main;
    String tag_id = "";
    String tag = "";
    TextView txt_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        totalCount = Integer.parseInt(getIntent().getStringExtra("totalCount"));
        tag_id = getIntent().getStringExtra("tag_id");
        tag = getIntent().getStringExtra("tag");
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        gv_img = findViewById(R.id.gv_img);
        imgv_type = findViewById(R.id.imgv_type);
        ll_main = findViewById(R.id.ll_main);
        txt_title = findViewById(R.id.txt_title);
    }

    private void setData() {
        imgv_type.setVisibility(View.GONE);
        imageData.clear();
        returnIds = "";
        txt_title.setText(tag);
        searchGridAdapter = new SearchGridAdapter(context, imageData);
        gv_img.setAdapter(searchGridAdapter);
        if (!appPrefrece.getAddFree()) {
            setAdMobBannerAds();
        }

        if (Utility.isNetworkAvailable(context)) {
            pd = Utility.showProgressDialog(context);
            getImageByTag();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);

        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position % 10 == 0 && position != 0 && !appPrefrece.getAddFree()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    }
                    if (position % 20 == 0) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        }
                    }
                }else{
                    Intent i = new Intent(context, CategoryViewImageActivity.class);
                    i.putExtra("position", position);
                    i.putExtra("tag_id", tag_id);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                }

            }
        });


        gv_img.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisibleRow = gv_img.getFirstVisiblePosition();
                int lastVisibleRow = gv_img.getLastVisiblePosition();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    if ((imageData.size() - 1) == i) {
                        if (isApiCall) {
                            isApiCall = false;
                            if (totalCount > (returnIds.split(",").length)) {
                                if (Utility.isNetworkAvailable(context)) {
                                    getImageByTag();
                                } else {
                                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (imageData.size() > 0) {
            searchGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }


    private void getImageByTag() {
        apiServer.getImageByTag(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = true;
                try {
                    if (object.getBoolean("success")) {
                        if (returnIds.equals("")) {
                            returnIds = object.getString("returnIds");
                        } else {
                            returnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {

                            if (imageData.size() % 10 == 0 && imageData.size() != 0 && !appPrefrece.getAddFree()) {
                                ImageData temp = new ImageData();
                                temp.setContainAds(true);
                                temp.setAdsLoded(false);
                                imageData.add(temp);
                            } else {
                                JSONObject image = object.getJSONArray("images").getJSONObject(i);
                                ImageData temp = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                temp.setContainAds(false);
                                imageData.add(temp);
                            }

                        }
                        searchGridAdapter.notifyDataSetChanged();
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, appPrefrece.getIsHorizontal() + "", appPrefrece.getUserId() + "", returnIds + "", tag_id + "");
    }


}
