package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.fragment.UploadImageFragment;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;


/**
 * Created by home on 12/07/18.
 */

public class AddImageActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    ImageView imgv_back;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    TextView txt_select_image, txt_upload_image, txt_select_tags;
    String final_path = "";
    RoundedImageView rimgv_upload;
    LinearLayout ll_select_tags;
    boolean is_able = false;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        txt_select_image = findViewById(R.id.txt_select_image);
        txt_upload_image = findViewById(R.id.txt_upload_image);
        rimgv_upload = findViewById(R.id.rimgv_upload);
        txt_select_tags = findViewById(R.id.txt_select_tags);
        ll_select_tags = findViewById(R.id.ll_select_tags);

    }

    private void setData() {
        txt_upload_image.setAlpha((float) 0.5);
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);
        txt_select_image.setOnClickListener(this);
        txt_upload_image.setOnClickListener(this);
        ll_select_tags.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        } else if (view == txt_select_image) {
            if (Utility.isNetworkAvailable(context)) {
                fromCameraGallery();
            } else {
                Utility.errDialog(context.getResources().getString(R.string.network), context);
            }
        } else if (view == ll_select_tags) {
            Utility.gotoNext(context, SelectTagsActivity.class);
        } else if (view == txt_upload_image) {
            if (is_able) {
                if (Utility.isNetworkAvailable(context)) {
                    addImage();
                } else {
                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                }
            }
        }
    }

    private void addImage() {
        pd = Utility.showProgressDialog(context);
        String ids = "";
        for (int i = 0; i < SelectTagsActivity.tagdIds.split(",").length; i++) {
            if (!SelectTagsActivity.tagdIds.split(",")[i].equals("")) {
                if (ids.equals("")) {
                    ids = SelectTagsActivity.tagdIds.split(",")[i];
                } else {
                    ids += "," + SelectTagsActivity.tagdIds.split(",")[i];
                }
            }
        }
        Log.e("Tag -> ", "ids -> " + ids);
        apiServer.addImage(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                Log.e("Tag -> ", object.toString());
                try {
                    if (object.getBoolean("success")) {
                        JSONObject image=object.getJSONObject("image");
                        ImageData temp = new ImageData(
                                image.getString("uploded_by_user_id") + "",
                                image.getString("o_image") + "",
                                image.getString("total_like") + "",
                                image.getString("is_active") + "",
                                image.getString("is_dwnld") + "",
                                image.getString("uploded_by_user_name") + "",
                                image.getString("is_fav") + "",
                                image.getString("uploded_by_user_photo") + "",
                                image.getString("img_id") + "",
                                image.getString("s_image") + "",
                                image.getString("is_premium") + "",
                                image.getString("is_like") + "",
                                image.getString("m_image") + "",
                                image.getString("premium_score") + "",
                                image.getString("total_dwnld") + "",
                                image.getString("total_fav") + "");
                        temp.setContainAds(false);
                        UploadImageFragment.imagesPending.add(temp);
                        onBackPressed();
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, new File(final_path), appPrefrece.getUserId(), ids, appPrefrece.getIsHorizontal());
    }

    private void fromCameraGallery() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);

        alertDialog2.setTitle("Select from..");
        alertDialog2.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utility.openCamera(context);
                        dialog.cancel();
                    }
                });
        alertDialog2.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utility.openGallry(context);
                        dialog.cancel();
                    }
                });

        alertDialog2.show();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GlobalAppConfiguration.RESULT_TAKE) {
                final_path = "";
                File f = new File(String.valueOf((Utility.camera)));
                ImageLoader.getInstance().displayImage("file://" + f, rimgv_upload, Utility.getImageOptions());
                if (f.exists()) {
                    final_path = Utility.camera + "";
                    if (!SelectTagsActivity.tagdIds.equals("")) {
                        is_able = true;
                        txt_upload_image.setAlpha(1);
                    }
                }
            } else if (requestCode == GlobalAppConfiguration.RESULT_GALLARY) {
                final_path = "";
                Uri uri = data.getData();
                String path = null;
                File f = null;
                try {
                    path = Utility.getRealPathFromURI(context, uri);
                    f = new File(path);
                    ImageLoader.getInstance().displayImage("file://" + f, rimgv_upload, Utility.getImageOptions());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (path != null) {
                    final_path = path;
                    if (!SelectTagsActivity.tagdIds.equals("")) {
                        is_able = true;
                        txt_upload_image.setAlpha(1);
                    }
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SelectTagsActivity.tagdIds.equals("")) {
            txt_select_tags.setText("- Select Tags -");
        } else {
            String text = "";
            for (int i = 0; i < SelectTagsActivity.tagSelectionData.size(); i++) {
                if (SelectTagsActivity.tagSelectionData.get(i).isSelected()) {
                    if (text.equals("")) {
                        text = SelectTagsActivity.tagSelectionData.get(i).getTag();
                    } else {
                        text += "," + SelectTagsActivity.tagSelectionData.get(i).getTag();
                    }
                }
            }
            txt_select_tags.setText(text);
            if (!final_path.equals("")) {
                is_able = true;
                txt_upload_image.setAlpha(1);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }
}
