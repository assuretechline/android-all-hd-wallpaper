package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by home on 12/07/18.
 */

public class WatchVideoAdsActivity extends BaseActivity implements View.OnClickListener, RewardedVideoAdListener {
    Context context;
    ImageView imgv_back;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    TextView txt_coin, txt_watch, txt_watch_free, txt_remove_add, txt_interval_add;
    ProgressDialog pd;
    boolean watchFromFree = false;
    private RewardedVideoAd mRewardedVideoAd, mRewardedVideoAd2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_video_ads);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        txt_coin = findViewById(R.id.txt_coin);
        txt_watch = findViewById(R.id.txt_watch);
        txt_watch_free = findViewById(R.id.txt_watch_free);
        txt_remove_add = findViewById(R.id.txt_remove_add);
        txt_interval_add = findViewById(R.id.txt_interval_add);
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        mRewardedVideoAd2 = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd2.setRewardedVideoAdListener(this);
    }

    private void setData() {

        txt_watch.setText("Loading...");
        txt_watch_free.setText("Loading...");

        mRewardedVideoAd.loadAd(appPrefrece.getREWARD_ID(), new AdRequest.Builder().build());
        mRewardedVideoAd2.loadAd(appPrefrece.getREWARD_ID(), new AdRequest.Builder().build());

        if (appPrefrece.getAddFree()) {
            startTimerForAdsFree();
        }
        if (appPrefrece.getAddInterval()) {
            startTimerForAdsInterval();
        }
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);
        txt_watch.setOnClickListener(this);
        txt_watch_free.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        } else if (view == txt_watch) {
            if (appPrefrece.getLogin()) {
                if (txt_watch.getText().toString().equalsIgnoreCase("watch")) {
                    watchFromFree = false;
                    mRewardedVideoAd.show();
                }
            } else {
                Intent i = new Intent(context, LoginActivity.class);
                i.putExtra("fromApp", true);
                context.startActivity(i);
                ((Activity) context).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
            }
        } else if (view == txt_watch_free) {
            if (txt_watch_free.getText().toString().equalsIgnoreCase("watch")) {
                watchFromFree = true;
                mRewardedVideoAd2.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        //Log.e("Tag -> ", "onRewardedVideoAdLoaded");
        if (mRewardedVideoAd2.isLoaded()) {
            txt_watch_free.setText("Watch");
        }
        if (mRewardedVideoAd.isLoaded()) {
            txt_watch.setText("Watch");

        }
        //mRewardedVideoAd.show();
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.e("Tag -> ", "onRewardedVideoAdOpened");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.e("Tag -> ", "onRewardedVideoStarted");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.e("Tag -> ", "onRewardedVideoAdClosed");
        if (watchFromFree) {
            Utility.successDialog("Now enjoy application ad free for next 10 Minutes.", context);
            mRewardedVideoAd2.loadAd(appPrefrece.getREWARD_ID(), new AdRequest.Builder().build());
        } else {
            Utility.successDialog("You can watch another reward video after 10 Minutes.", context);
            mRewardedVideoAd.loadAd(appPrefrece.getREWARD_ID(), new AdRequest.Builder().build());
        }
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.e("Tag -> ", "onRewarded");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.e("Tag -> ", "onRewardedVideoAdLeftApplication");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.e("Tag -> ", "onRewardedVideoAdFailedToLoad");
    }

    @Override
    public void onRewardedVideoCompleted() {
        Log.e("Tag -> ", "onRewardedVideoCompleted");
        if (watchFromFree) {
            txt_watch_free.setText("Loading...");
            //startService(new Intent(this, TimerService.class));
            appPrefrece.setAddFree(true);
            appPrefrece.setTimeForFree(600000);
            GlobalAppConfiguration.startTimer();
            txt_watch_free.setVisibility(View.GONE);
            startTimerForAdsFree();
            adSeenForAdFree();
            if (MainActivity.getInstance().homeFragment.premiumFragment != null)
                MainActivity.getInstance().homeFragment.premiumFragment.setData();
        } else {
            changeUserScore("10");
            txt_watch.setText("Loading...");
            appPrefrece.setAddInterval(true);
            appPrefrece.setTimeForInterval(600000);
            GlobalAppConfiguration.startTimerForInterval();
            txt_watch.setVisibility(View.GONE);
            startTimerForAdsInterval();
        }
    }

    @Override
    public void onResume() {
        if (watchFromFree) {
            mRewardedVideoAd2.resume(this);
        } else {
            mRewardedVideoAd.resume(this);
        }
        String coin = "0";
        try {
            coin = appPrefrece.getProfile().getString("user_score");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        txt_coin.setText(coin);
        super.onResume();
    }

    @Override
    public void onPause() {
        if (watchFromFree) {
            mRewardedVideoAd2.pause(this);
        } else {
            mRewardedVideoAd.pause(this);
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (watchFromFree) {
            mRewardedVideoAd2.destroy(this);
        } else {
            mRewardedVideoAd.destroy(this);
        }
        super.onDestroy();
    }

    private void changeUserScore(String score) {
        pd = Utility.showProgressDialog(context);
        apiServer.changeUserScore(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        appPrefrece.setProfile(object.getJSONObject("user"));
                        txt_coin.setText(object.getJSONObject("user").getString("user_score"));
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                    }
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d = df.parse(object.getString("servertime"));
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.MINUTE, 10);
                    appPrefrece.setEndTimeForInterval(df.format(cal.getTime()));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, "1", score + "", appPrefrece.getUserId());
    }

    private void adSeenForAdFree() {
        apiServer.adSeenForAdFree(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d = df.parse(object.getString("servertime"));
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.MINUTE, 10);
                    appPrefrece.setEndTimeForFree(df.format(cal.getTime()));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
            }
        });
    }

    public void startTimerForAdsFree() {
        txt_watch_free.setVisibility(View.GONE);
        new CountDownTimer(appPrefrece.getTimeForFree(), 1000) {
            public void onTick(long millisUntilFinished) {
                //appPrefrece.setTimeForFree(millisUntilFinished);
                long minutes = (millisUntilFinished / 1000) / 60;
                long seconds = (millisUntilFinished / 1000) % 60;
                if (minutes == 0) {
                    txt_remove_add.setText("Please wait for " + seconds + " seconds");
                } else {
                    txt_remove_add.setText("Please wait for " + minutes + " minutes,\n" + seconds + " seconds");
                }
            }

            public void onFinish() {
                if (MainActivity.getInstance().homeFragment.premiumFragment != null)
                    MainActivity.getInstance().homeFragment.premiumFragment.setData();
                txt_remove_add.setText(context.getResources().getText(R.string.remove_ad));
                txt_watch_free.setVisibility(View.VISIBLE);
                appPrefrece.setTimeForFree(0);
                appPrefrece.setAddFree(false);
            }
        }.start();
    }

    public void startTimerForAdsInterval() {
        txt_watch.setVisibility(View.GONE);
        new CountDownTimer(appPrefrece.getTimeForInterval(), 1000) {
            public void onTick(long millisUntilFinished) {
                long minutes = (millisUntilFinished / 1000) / 60;
                long seconds = (millisUntilFinished / 1000) % 60;
                if (minutes == 0) {
                    txt_interval_add.setText("Next reward video will be available after " + seconds + " seconds");
                } else {
                    txt_interval_add.setText("Next reward video will be available after " + minutes + " minutes, " + seconds + " seconds");
                }
            }

            public void onFinish() {
                txt_interval_add.setText(context.getResources().getText(R.string.watch_earn));
                txt_watch.setVisibility(View.VISIBLE);
                appPrefrece.setTimeForInterval(0);
                appPrefrece.setAddInterval(false);
            }
        }.start();
    }
}
