package com.wallpaperzilla.wallpaper.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.adapter.TagSelectionAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.TagSelectionData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 12/07/18.
 */

public class SelectTagsActivity extends BaseActivity implements View.OnClickListener {
    public static String tagdIds = "";
    public static ArrayList<TagSelectionData> tagSelectionData = new ArrayList<>();
    Context context;
    ImageView imgv_back, imgv_add;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ListView lstv_tags;
    EditText edt_search;
    TextView txt_done;
    ProgressDialog pd;
    TagSelectionAdapter tagSelectionAdapter;
    String oldTagIds = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_tags);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        oldTagIds = tagdIds;
        if (tagdIds.equals("")) {
            for (int i = 0; i < tagSelectionData.size(); i++) {
                tagSelectionData.get(i).setSelected(false);
            }
        }
        tagSelectionAdapter = new TagSelectionAdapter(context, tagSelectionData);

    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        imgv_add = findViewById(R.id.imgv_add);
        edt_search = findViewById(R.id.edt_search);
        txt_done = findViewById(R.id.txt_done);
        lstv_tags = findViewById(R.id.lstv_tags);
        lstv_tags.setAdapter(tagSelectionAdapter);
    }

    private void setData() {
        if (Utility.isNetworkAvailable(context)) {
            if (tagSelectionData.size() == 0)
                getAllTag();
        } else {
            Utility.errDialog(context.getResources().getString(R.string.network), context);
        }
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);
        imgv_add.setOnClickListener(this);
        txt_done.setOnClickListener(this);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tagSelectionAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        } else if (view == txt_done) {
            Utility.gotoBack(context);
        } else if (view == imgv_add) {
            //insertTagDialog();
            showCustomInsertTagDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        tagdIds = oldTagIds;
        Utility.gotoBack(context);
    }

    private void getAllTag() {
        pd = Utility.showProgressDialog(context);
        apiServer.getAllTag(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        tagSelectionData.clear();
                        for (int i = 0; i < object.getJSONArray("tags").length(); i++) {
                            JSONObject temp = object.getJSONArray("tags").getJSONObject(i);
                            boolean selected = tagIdsAvialable(temp.getString("tag_id"));
                            if (selected) {
                                tagSelectionData.add(0, new TagSelectionData(temp.getString("tag_id"), temp.getString("tag"), selected));
                            } else {
                                tagSelectionData.add(new TagSelectionData(temp.getString("tag_id"), temp.getString("tag"), selected));
                            }
                        }
                        tagSelectionAdapter.notifyDataSetChanged();
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Log.e("Tag -> ", e.getMessage());
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        });
    }

    private void insertSingleTag(String tag) {
        pd = Utility.showProgressDialog(context);
        apiServer.insertSingleTag(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        Log.e("Tag -> ", object.toString());
                        JSONObject temp = object.getJSONObject("tag");
                        if (SelectTagsActivity.tagdIds.split(",").length != 6) {
                            tagSelectionData.add(0, new TagSelectionData(temp.getString("tag_id"), temp.getString("tag"), true));
                        } else {
                            tagSelectionData.add(new TagSelectionData(temp.getString("tag_id"), temp.getString("tag"), false));
                        }
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Log.e("Tag -> ", e.getMessage());
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, tag);
    }

    private void showCustomInsertTagDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_custom_insert_tag);
        dialog.setCancelable(false);
        LinearLayout ll_main = dialog.findViewById(R.id.ll_main);
        ll_main.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen._270sdp);
        final EditText edt_input = dialog.findViewById(R.id.edt_input);
        TextView txt_no = dialog.findViewById(R.id.txt_no);
        TextView txt_yes = dialog.findViewById(R.id.txt_yes);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edt_input.getText().toString().trim().equals("")) {

                    if (tagAlreadyAvialable(edt_input.getText().toString().trim())) {
                        Utility.errDialog("Tag already avialable in list.", context);
                    } else {
                        insertSingleTag(edt_input.getText().toString());
                        dialog.dismiss();
                    }

                } else {
                    Utility.errDialog("Please write tag name.", context);
                }
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public boolean tagIdsAvialable(String id) {
        boolean avialable = false;
        for (int i = 0; i < tagdIds.split(",").length; i++) {
            if (id.equals(tagdIds.split(",")[i])) {
                avialable = true;
                break;
            }
        }
        return avialable;
    }

    public boolean tagAlreadyAvialable(String tag) {
        boolean avialable = false;
        for (int i = 0; i < tagSelectionData.size(); i++) {
            if (tag.toLowerCase().equals(tagSelectionData.get(i).getTag().toLowerCase())) {
                avialable = true;
                break;
            }
        }
        return avialable;
    }

}
