package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.adapter.SearchGridAdapter;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 12/07/18.
 */

public class SearchByWordActivity extends BaseActivity implements View.OnClickListener {
    public static String returnIds = "";
    public static ArrayList<ImageData> imageData = new ArrayList<>();
    public static int totalCount = 0;
    public static String s_text = "";
    Context context;
    ImageView imgv_back, imgv_clear, imgv_type;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    EditText edt_search;
    ProgressDialog pd;
    GridView gv_img;
    boolean isApiCall = true;
    SearchGridAdapter searchGridAdapter;
    LinearLayout ll_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_word);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);
        imgv_clear = findViewById(R.id.imgv_clear);
        edt_search = findViewById(R.id.edt_search);
        gv_img = findViewById(R.id.gv_img);
        imgv_type = findViewById(R.id.imgv_type);
        ll_main = findViewById(R.id.ll_main);
    }

    private void setData() {
        imgv_clear.setVisibility(View.GONE);
        imgv_type.setVisibility(View.GONE);
        imageData.clear();
        returnIds = "";
        totalCount = 0;
        s_text = "";
        searchGridAdapter = new SearchGridAdapter(context, imageData);
        gv_img.setAdapter(searchGridAdapter);

        if(!appPrefrece.getAddFree()){
            setAdMobBannerAds();
        }
    }

    public void setAdMobBannerAds() {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        ll_main.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setLitionar() {
        imgv_back.setOnClickListener(this);
        imgv_clear.setOnClickListener(this);

        gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position % 10 == 0 && position != 0 && !appPrefrece.getAddFree()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink())));
                    }
                    if (position % 20 == 0) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPrefrece.getAdsLink2())));
                        }
                    }
                }else{
                    Intent i = new Intent(context, SearchByWordImageActivity.class);
                    i.putExtra("position", position);
                    context.startActivity(i);
                    ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                }

            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 0) {
                    imgv_clear.setVisibility(View.GONE);
                } else {
                    imgv_clear.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (Utility.isNetworkAvailable(context)) {

                        if (edt_search.getText().toString().length() != 0) {
                            pd = Utility.showProgressDialog(context);
                            imageData.clear();
                            returnIds = "";
                            totalCount = 0;
                            searchGridAdapter.notifyDataSetChanged();
                            s_text = edt_search.getText().toString().toLowerCase().replace(" ", "");
                            imageSearchByWord();
                            return false;
                        } else {
                            Utility.errDialog("Please insert value.", context);
                            return true;
                        }
                    } else {
                        Utility.errDialog(context.getResources().getString(R.string.network), context);
                        return true;
                    }
                } else {
                    return false;
                }
            }
        });

        gv_img.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisibleRow = gv_img.getFirstVisiblePosition();
                int lastVisibleRow = gv_img.getLastVisiblePosition();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    if ((imageData.size() - 1) == i) {
                        if (isApiCall) {
                            isApiCall = false;
                            if (totalCount > (returnIds.split(",").length)) {
                                if (Utility.isNetworkAvailable(context)) {
                                    imageSearchByWord();
                                } else {
                                    Utility.errDialog(context.getResources().getString(R.string.network), context);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        } else if (view == imgv_clear) {
            edt_search.setText("");
            imageData.clear();
            returnIds = "";
            totalCount = 0;
            searchGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (imageData.size() > 0) {
            searchGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }

    private void imageSearchByWord() {

        apiServer.imageSearchByWord(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                isApiCall = true;
                try {
                    if (object.getBoolean("success")) {
                        totalCount = Integer.parseInt(object.getString("total_count"));
                        if (returnIds.equals("")) {
                            returnIds = object.getString("returnIds");
                        } else {
                            returnIds += "," + object.getString("returnIds");
                        }
                        for (int i = 0; i < object.getJSONArray("images").length(); i++) {

                            if (imageData.size() % 10 == 0 && imageData.size() != 0  && !appPrefrece.getAddFree()) {
                                ImageData temp = new ImageData();
                                temp.setContainAds(true);
                                temp.setAdsLoded(false);
                                imageData.add(temp);
                            } else {
                                JSONObject image = object.getJSONArray("images").getJSONObject(i);
                                ImageData temp = new ImageData(
                                        image.getString("uploded_by_user_id") + "",
                                        image.getString("o_image") + "",
                                        image.getString("total_like") + "",
                                        image.getString("is_active") + "",
                                        image.getString("is_dwnld") + "",
                                        image.getString("uploded_by_user_name") + "",
                                        image.getString("is_fav") + "",
                                        image.getString("uploded_by_user_photo") + "",
                                        image.getString("img_id") + "",
                                        image.getString("s_image") + "",
                                        image.getString("is_premium") + "",
                                        image.getString("is_like") + "",
                                        image.getString("m_image") + "",
                                        image.getString("premium_score") + "",
                                        image.getString("total_dwnld") + "",
                                        image.getString("total_fav") + "");
                                temp.setContainAds(false);
                                imageData.add(temp);
                            }

                        }
                        Utility.hideSoftKeyboard(edt_search,context);
                        searchGridAdapter.notifyDataSetChanged();
                        Utility.dismissProgressDialog(pd);
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, returnIds, appPrefrece.getIsHorizontal(), s_text, appPrefrece.getUserId());
    }

}
