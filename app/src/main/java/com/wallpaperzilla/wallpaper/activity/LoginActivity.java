package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.api.APIResponse;
import com.wallpaperzilla.wallpaper.api.APIServer;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


/**
 * Created by home on 12/07/18.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_SIGN_IN = 9001;
    Context context;
    ImageView imgv_close, imgv_facebook, imgv_google,imgv_bg;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    TextView txt_skip;
    CallbackManager mCallbackManager;
    FirebaseAuth firebaseAuth;
    GoogleApiClient mGoogleApiClient;
    LoginButton login_button;
    ProgressDialog pd;
    List<String> permissionNeeds = Arrays.asList("user_photos", "email", "public_profile");
    LinearLayout ll_fb,ll_google;

    boolean fromApp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
        setLogin();
        facebookSignIN();
        if(GlobalAppConfiguration.Main_Url.equals("")){
            setFirebaseData();
        }
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        fromApp = getIntent().getBooleanExtra("fromApp", false);
    }

    private void setView() {
        imgv_google = findViewById(R.id.imgv_google);
        imgv_close = findViewById(R.id.imgv_close);
        imgv_facebook = findViewById(R.id.imgv_facebook);
        imgv_bg = findViewById(R.id.imgv_bg);
        txt_skip = findViewById(R.id.txt_skip);
        ll_fb = findViewById(R.id.ll_fb);
        ll_google = findViewById(R.id.ll_google);
        login_button = (LoginButton) findViewById(R.id.login_button);
        login_button.setReadPermissions(permissionNeeds);
    }

    private void setData() {
        if (fromApp) {
            imgv_close.setVisibility(View.VISIBLE);
            txt_skip.setVisibility(View.GONE);
        } else {
            imgv_close.setVisibility(View.GONE);
            txt_skip.setVisibility(View.VISIBLE);
        }
    }

    private void setLitionar() {
        ll_google.setOnClickListener(this);
        imgv_close.setOnClickListener(this);
        ll_fb.setOnClickListener(this);
        txt_skip.setOnClickListener(this);
    }

    private void setColor() {
        imgv_google.setColorFilter(context.getResources().getColor(R.color.white_image));
        imgv_close.setColorFilter(context.getResources().getColor(R.color.login_yellow));
        imgv_facebook.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    private void setLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(LoginActivity.this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onClick(View view) {
        if (view == ll_google) {
            googleSignIN();
        } else if (view == ll_fb) {
            login_button.performClick();
        } else if (view == imgv_close) {
            txt_skip.performClick();
        } else if (view == txt_skip) {

            appPrefrece.setLogin(false);
            appPrefrece.setProfile(new JSONObject());
            appPrefrece.setUserId("0");
            appPrefrece.setIsHorizontal("0");

            if (fromApp) {
                onBackPressed();
            } else {
                Intent i = new Intent(context, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(i);
                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                finish();
            }
        }
    }

    private void googleSignIN() {
        Intent signIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signIntent, RC_SIGN_IN);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (fromApp) {
            ((Activity) context).finish();
            ((Activity) context).overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
        } else {
            Utility.gotoBack(context);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                authWithGoogle(account);
            } else {

            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void authWithGoogle(GoogleSignInAccount account) {
        pd = Utility.showProgressDialog(context);
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    FirebaseUser user = firebaseAuth.getCurrentUser();


                    String uid = "", phone = "", email = "", name = "", photo = "";
                    if (user.getUid() != null) {
                        uid = user.getUid();
                    }
                    if (user.getPhoneNumber() != null) {
                        phone = user.getPhoneNumber();
                    }
                    if (user.getEmail() != null) {
                        email = user.getEmail();
                    }
                    if (user.getDisplayName() != null) {
                        name = user.getDisplayName();
                    }
                    if (user.getPhotoUrl() != null) {
                        photo = user.getPhotoUrl().toString();
                    }

                    Utility.dismissProgressDialog(pd);
                    if (Utility.isNetworkAvailable(context)) {
                        logIn("0", uid, phone, email, name, photo);
                    } else {
                        Utility.errDialog(context.getResources().getString(R.string.network), context);
                    }


                } else {
                    Utility.dismissProgressDialog(pd);
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void facebookSignIN() {
        LoginManager.getInstance().logOut();
        login_button.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        login_button.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {

                                    String uid = "", phone = "", email = "", name = "", photo = "";

                                    uid = object.getString("id");
                                    name = object.getString("name");
                                    email = object.getString("email");
                                    photo = object.getJSONObject("picture").getJSONObject("data").getString("url");


                                    if (Utility.isNetworkAvailable(context)) {
                                        logIn("1", uid, phone, email, name, photo);
                                    } else {
                                        Utility.errDialog(context.getResources().getString(R.string.network), context);
                                    }
                                } catch (JSONException e) {

                                    e.printStackTrace();
                                }

                                /*String email = object.getString("email");
                                String birthday = object.getString("birthday"); // 01/31/1980 format*/
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
                //handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    private void logIn(final String is_fb,final String social_id,final String phone,final String email, final String name,final String photo) {
        pd = Utility.showProgressDialog(context);
        SharedPreferences pref = context.getSharedPreferences(GlobalAppConfiguration.SHARED_PREF, 0);
        String device_id = pref.getString("regId", "");

        apiServer.logIn(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getBoolean("success")) {
                        JSONObject userObject = object.getJSONObject("user");
                        appPrefrece.setLogin(true);
                        appPrefrece.setProfile(userObject);
                        appPrefrece.setUserId(userObject.getString("user_id"));
                        appPrefrece.setIsHorizontal("0");

                        appPrefrece.setIS_FB(is_fb);
                        appPrefrece.setSOCIAL_ID(social_id);
                        appPrefrece.setPHONE(phone);
                        appPrefrece.setEMAIL(email);
                        appPrefrece.setNAME(name);
                        appPrefrece.setPHOTO(photo);

                        Utility.dismissProgressDialog(pd);

                        if (fromApp) {
                            onBackPressed();
                        } else {
                            Intent i = new Intent(context, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(i);
                            overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                            finish();
                        }
                    } else {
                        Utility.dismissProgressDialog(pd);
                        Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
            }
        }, is_fb, social_id, phone, email, name, photo, "",Utility.getDeviceId(context),"","","","","","");
    }

    private void setFirebaseData() {
        if (Utility.isNetworkAvailable(context)) {
            try {
                pd=Utility.showProgressDialog(context);
                FirebaseApp.initializeApp(context);
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        appPrefrece.setAPP_ID(dataSnapshot.child("android_go_appID").getValue().toString());
                        appPrefrece.setBANNER_ID(dataSnapshot.child("android_go_banner").getValue().toString());
                        appPrefrece.setREWARD_ID(dataSnapshot.child("android_go_reward").getValue().toString());
                        appPrefrece.setINTESTRIAL_ID(dataSnapshot.child("android_go_in").getValue().toString());
                        appPrefrece.setFB_INTESTRIAL_ID(dataSnapshot.child("android_fb_in").getValue().toString());

                     /*   appPrefrece.setAPP_ID(dataSnapshot.child("test_android_go_appID").getValue().toString());
                        appPrefrece.setBANNER_ID(dataSnapshot.child("test_android_go_banner").getValue().toString());
                        appPrefrece.setREWARD_ID(dataSnapshot.child("test_android_go_reward").getValue().toString());
                        appPrefrece.setINTESTRIAL_ID(dataSnapshot.child("test_android_go_in").getValue().toString());
                        appPrefrece.setFB_INTESTRIAL_ID(dataSnapshot.child("test_android_fb_in").getValue().toString());*/

                        GlobalAppConfiguration.Main_Url=dataSnapshot.child("main_url").getValue().toString();

                        Utility.dismissProgressDialog(pd);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utility.dismissProgressDialog(pd);
                        networkTryAdain();
                    }
                });
            } catch (Exception e) {
                Utility.dismissProgressDialog(pd);
                networkTryAdain();
            }
        } else {
            networkTryAdain();
        }
    }

    private void networkTryAdain() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Alert !");
        alertDialog2.setCancelable(false);
        alertDialog2.setMessage(context.getResources().getString(R.string.network));
        alertDialog2.setPositiveButton("Try Again",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setFirebaseData();
                        dialog.cancel();
                    }
                });
        alertDialog2.show();
    }
}
