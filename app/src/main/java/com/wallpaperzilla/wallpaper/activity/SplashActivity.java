package com.wallpaperzilla.wallpaper.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.IOException;

import pl.droidsonroids.gif.GifImageView;


public class SplashActivity extends BaseActivity {
    Context context;
    ImageView imgv_logo;
    ImageLoader imageLoader;
    AppPrefrece appPrefrece;
    GifImageView imgv_gif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialize();
        setView();
        setData();
    }

    private void initialize() {
        context = this;
        appPrefrece = new AppPrefrece(context);
        Utility.setStatusColor(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView() {
        imgv_logo = findViewById(R.id.imgv_logo);
        imgv_gif = findViewById(R.id.imgv_gif);

        imageLoader.displayImage("drawable://" + R.drawable.icon, imgv_logo);
    }

    private void setData() {
        if (Utility.isNetworkAvailable(context)) {
            try {
                imgv_gif.setVisibility(View.VISIBLE);
                FirebaseApp.initializeApp(context);
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //Log.e("Tag","dataSnapshot : \n \n "+dataSnapshot.toString()+" \n \n");

                        appPrefrece.setAPP_ID(dataSnapshot.child("android_go_appID").getValue().toString());
                        appPrefrece.setBANNER_ID(dataSnapshot.child("android_go_banner").getValue().toString());
                        appPrefrece.setREWARD_ID(dataSnapshot.child("android_go_reward").getValue().toString());
                        appPrefrece.setINTESTRIAL_ID(dataSnapshot.child("android_go_in").getValue().toString());
                        appPrefrece.setFB_INTESTRIAL_ID(dataSnapshot.child("android_fb_in").getValue().toString());

                        /*appPrefrece.setAPP_ID(dataSnapshot.child("test_android_go_appID").getValue().toString());
                        appPrefrece.setBANNER_ID(dataSnapshot.child("test_android_go_banner").getValue().toString());
                        appPrefrece.setREWARD_ID(dataSnapshot.child("test_android_go_reward").getValue().toString());
                        appPrefrece.setINTESTRIAL_ID(dataSnapshot.child("test_android_go_in").getValue().toString());
                        appPrefrece.setFB_INTESTRIAL_ID(dataSnapshot.child("test_android_fb_in").getValue().toString());*/

                        GlobalAppConfiguration.Main_Url=dataSnapshot.child("main_url").getValue().toString();

                        imgv_gif.setVisibility(View.GONE);

                        if (appPrefrece.getLogin()) {
                            Utility.gotoNext(context, MainActivity.class);
                        } else {
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.putExtra("fromApp", false);
                            context.startActivity(intent);
                            ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                        }
                        finish();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        imgv_gif.setVisibility(View.GONE);
                        networkTryAdain();
                    }
                });
            } catch (Exception e) {
                Log.e("Tag ","Firebase exception : "+e.getMessage());
                imgv_gif.setVisibility(View.GONE);
                networkTryAdain();
            }
        } else {
            networkTryAdain();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void networkTryAdain() {

        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Alert !");
        alertDialog2.setCancelable(false);
        alertDialog2.setMessage(context.getResources().getString(R.string.network));
        alertDialog2.setPositiveButton("Try Again",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setData();
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }
}
