package com.wallpaperzilla.wallpaper.model;

public class TagData {
	private ImageData imageData;
	private String imageTotalCount;
	private String tagId;
	private String tag;

	public TagData(ImageData imageData, String imageTotalCount, String tagId, String tag) {
		this.imageData = imageData;
		this.imageTotalCount = imageTotalCount;
		this.tagId = tagId;
		this.tag = tag;
	}

	public void setImageData(ImageData imageData){
		this.imageData = imageData;
	}

	public ImageData getImageData(){
		return imageData;
	}

	public void setImageTotalCount(String imageTotalCount){
		this.imageTotalCount = imageTotalCount;
	}

	public String getImageTotalCount(){
		return imageTotalCount;
	}

	public void setTagId(String tagId){
		this.tagId = tagId;
	}

	public String getTagId(){
		return tagId;
	}

	public void setTag(String tag){
		this.tag = tag;
	}

	public String getTag(){
		return tag;
	}

}
