package com.wallpaperzilla.wallpaper.model;

import java.io.Serializable;

public class ImageData implements Serializable {
    private String uplodedByUserId;
    private String oImage;
    private String totalLike;
    private String isActive;
    private String isDwnld;
    private String uplodedByUserName;
    private String isFav;
    private String uplodedByUserPhoto;
    private String imgId="0";
    private String sImage;
    private String isPremium;
    private String isLike;
    private String mImage;
    private String premiumScore;
    private String totalDwnld;
    private String totalFav;
    private boolean containAds=false;
    private boolean adsLoded=false;

    public ImageData() { }

    public ImageData(String uplodedByUserId, String oImage, String totalLike, String isActive, String isDwnld, String uplodedByUserName, String isFav, String uplodedByUserPhoto, String imgId, String sImage, String isPremium, String isLike, String mImage, String premiumScore, String totalDwnld, String totalFav) {
        this.uplodedByUserId = uplodedByUserId;
        this.oImage = oImage;
        this.totalLike = totalLike;
        this.isActive = isActive;
        this.isDwnld = isDwnld;
        this.uplodedByUserName = uplodedByUserName;
        this.isFav = isFav;
        this.uplodedByUserPhoto = uplodedByUserPhoto;
        this.imgId = imgId;
        this.sImage = sImage;
        this.isPremium = isPremium;
        this.isLike = isLike;
        this.mImage = mImage;
        this.premiumScore = premiumScore;
        this.totalDwnld = totalDwnld;
        this.totalFav = totalFav;
    }

    public boolean isAdsLoded() {
        return adsLoded;
    }

    public void setAdsLoded(boolean adsLoded) {
        this.adsLoded = adsLoded;
    }

    public boolean isContainAds() {
        return containAds;
    }

    public void setContainAds(boolean containAds) {
        this.containAds = containAds;
    }

    public String getUplodedByUserId() {
        return uplodedByUserId;
    }

    public void setUplodedByUserId(String uplodedByUserId) {
        this.uplodedByUserId = uplodedByUserId;
    }

    public String getOImage() {
        return oImage;
    }

    public void setOImage(String oImage) {
        this.oImage = oImage;
    }

    public String getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(String totalLike) {
        this.totalLike = totalLike;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsDwnld() {
        return isDwnld;
    }

    public void setIsDwnld(String isDwnld) {
        this.isDwnld = isDwnld;
    }

    public String getUplodedByUserName() {
        return uplodedByUserName;
    }

    public void setUplodedByUserName(String uplodedByUserName) {
        this.uplodedByUserName = uplodedByUserName;
    }

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public String getUplodedByUserPhoto() {
        return uplodedByUserPhoto;
    }

    public void setUplodedByUserPhoto(String uplodedByUserPhoto) {
        this.uplodedByUserPhoto = uplodedByUserPhoto;
    }

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public String getSImage() {
        return sImage;
    }

    public void setSImage(String sImage) {
        this.sImage = sImage;
    }

    public String getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(String isPremium) {
        this.isPremium = isPremium;
    }

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public String getMImage() {
        return mImage;
    }

    public void setMImage(String mImage) {
        this.mImage = mImage;
    }

    public String getPremiumScore() {
        return premiumScore;
    }

    public void setPremiumScore(String premiumScore) {
        this.premiumScore = premiumScore;
    }

    public String getTotalDwnld() {
        return totalDwnld;
    }

    public void setTotalDwnld(String totalDwnld) {
        this.totalDwnld = totalDwnld;
    }

    public String getTotalFav() {
        return totalFav;
    }

    public void setTotalFav(String totalFav) {
        this.totalFav = totalFav;
    }

}
