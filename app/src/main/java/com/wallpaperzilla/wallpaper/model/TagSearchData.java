package com.wallpaperzilla.wallpaper.model;

public class TagSearchData {
	private String total;
	private String tagId;
	private String createdAt;
	private String tag;
	private boolean selected;

	public TagSearchData(String total, String tagId, String createdAt, String tag, boolean selected) {
		this.total = total;
		this.tagId = tagId;
		this.createdAt = createdAt;
		this.tag = tag;
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setTagId(String tagId){
		this.tagId = tagId;
	}

	public String getTagId(){
		return tagId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setTag(String tag){
		this.tag = tag;
	}

	public String getTag(){
		return tag;
	}

}
