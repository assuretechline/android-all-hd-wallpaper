package com.wallpaperzilla.wallpaper.model;

public class TagSelectionData {
    String tag_id,tag;
    boolean selected;

    public TagSelectionData(String tag_id, String tag, boolean selected) {
        this.tag_id = tag_id;
        this.tag = tag;
        this.selected = selected;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
