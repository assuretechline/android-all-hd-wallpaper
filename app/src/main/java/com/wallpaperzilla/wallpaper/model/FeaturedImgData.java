package com.wallpaperzilla.wallpaper.model;

public class FeaturedImgData {
    String id,thumbSmall,thumbBig,imgUrl,profileImg,name,credit,like,download,image_total_count;

    public FeaturedImgData(String id, String thumbSmall, String thumbBig, String imgUrl) {
        this.id = id;
        this.thumbSmall = thumbSmall;
        this.thumbBig = thumbBig;
        this.imgUrl = imgUrl;
    }

    public FeaturedImgData(String id, String thumbSmall, String thumbBig, String imgUrl, String profileImg, String name, String credit, String like, String download) {
        this.id = id;
        this.profileImg = profileImg;
        this.name = name;
        this.credit = credit;
        this.thumbSmall = thumbSmall;
        this.thumbBig = thumbBig;
        this.imgUrl = imgUrl;
        this.like = like;
        this.download = download;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbSmall() {
        return thumbSmall;
    }

    public void setThumbSmall(String thumbSmall) {
        this.thumbSmall = thumbSmall;
    }

    public String getThumbBig() {
        return thumbBig;
    }

    public void setThumbBig(String thumbBig) {
        this.thumbBig = thumbBig;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }
}
