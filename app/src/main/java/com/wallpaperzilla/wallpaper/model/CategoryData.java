package com.wallpaperzilla.wallpaper.model;

public class CategoryData {
    String id, title, img;

    String tag_id, tag, image_total_count, tag_photo;


    public CategoryData(String tag_id, String tag, String image_total_count, String tag_photo) {
        this.tag_id = tag_id;
        this.tag = tag;
        this.image_total_count = image_total_count;
        this.tag_photo = tag_photo;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getImage_total_count() {
        return image_total_count;
    }

    public void setImage_total_count(String image_total_count) {
        this.image_total_count = image_total_count;
    }

    public String getTag_photo() {
        return tag_photo;
    }

    public void setTag_photo(String tag_photo) {
        this.tag_photo = tag_photo;
    }
}
