package com.wallpaperzilla.wallpaper.model;

/**
 * Created by Amisha on 21-Feb-18.
 */

public class MenuData {
    String title;
    boolean selecetd=false;

    public MenuData(String title, boolean selecetd) {
        this.title = title;
        this.selecetd = selecetd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelecetd() {
        return selecetd;
    }

    public void setSelecetd(boolean selecetd) {
        this.selecetd = selecetd;
    }
}
