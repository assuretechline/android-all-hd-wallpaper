package com.wallpaperzilla.wallpaper.api;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * This utility class provides an abstraction layer for sending multipart HTTP
 * POST requests to a web server.
 *
 * @author www.codejava.net
 */
public class MultipartUtility {
    private final String boundary = "*****";
    private final String twoHyphens = "--";
    private static final String LINE_FEED = "\r\n";
    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1024 * 1024;
    private HttpURLConnection connection;
    FileInputStream fileInputStream;
    DataOutputStream outputStream;

    private String charset;
    private OutputStream oldOutputStream;
    private PrintWriter writer;

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     *
     * @param requestURL
     * @param charset
     * @throws IOException
     */
    public MultipartUtility(String requestURL, String charset)
            throws IOException {
        this.charset = charset;

        // creates a unique boundary based on time stamp


        URL url = new URL(requestURL);
        connection = (HttpURLConnection) url.openConnection();
        connection.setUseCaches(false);
        connection.setDoOutput(true); // indicates POST method
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + boundary);
        connection.setRequestProperty("Accept", "*/*");
        oldOutputStream = connection.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(oldOutputStream, charset), true);


        //-----------

       /* URL url = new URL(requestURL);
        connection = (HttpURLConnection) url.openConnection();

        // Allow Inputs &amp; Outputs.
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);

        // Set HTTP method to POST.
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

        outputStream = new DataOutputStream(connection.getOutputStream());*/

    }

    /**
     * Adds a form field to the request
     *
     * @param name  field name
     * @param value field value
     */
    public void addFormField(String name, String value) {
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + name + "\"")
                .append(LINE_FEED);
        writer.append("Content-Type: text/plain; charset=" + charset).append(
                LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Adds a upload file section to the request
     *
     * @param fieldName  name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        String fileName = uploadFile.getName();
        //Log.e("Tag -> ", "fileName -> " + fileName);
        //Log.e("Tag -> ", "uploadFile -> " + uploadFile);
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append(
                "Content-Disposition: form-data; name=\"" + fieldName
                        + "\"; filename=\"" + fileName + "\"")
                .append(LINE_FEED);
        writer.append(
                "Content-Type: "
                        + URLConnection.guessContentTypeFromName(fileName))
                .append(LINE_FEED);
        // writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[2048];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            oldOutputStream.write(buffer, 0, bytesRead);
        }
        oldOutputStream.flush();
        inputStream.close();

        writer.append(LINE_FEED);
        writer.flush();

    }


    /**
     * Adds a upload file section to the request
     *
     * @param path a String to be uploaded
     * @throws IOException
     */
    public void addFilePartNewWay(String path)
            throws IOException {

        outputStream.writeBytes(twoHyphens + boundary + LINE_FEED);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"id\"" + LINE_FEED);
        outputStream.writeBytes(LINE_FEED);
        outputStream.writeBytes("123");
        outputStream.writeBytes(LINE_FEED);


        outputStream.writeBytes(twoHyphens + boundary + LINE_FEED);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"tablename\"" + LINE_FEED);
        outputStream.writeBytes(LINE_FEED);
        outputStream.writeBytes("for_test");
        outputStream.writeBytes(LINE_FEED);


        outputStream.writeBytes(twoHyphens + boundary + LINE_FEED);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"img\";filename=\"" + new File(path).getName() + "\"" + LINE_FEED);
        outputStream.writeBytes(LINE_FEED);


        fileInputStream = new FileInputStream(path);
        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];
        // Read file
        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        outputStream.writeBytes(LINE_FEED);
        outputStream.writeBytes(twoHyphens + boundary + twoHyphens + LINE_FEED);
    }


    /**
     * Adds a header field to the request.
     *
     * @param name  - name of the header field
     * @param value - value of the header field
     */
    public void addHeaderField(String name, String value) {
        writer.append(name + ": " + value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Completes the request and receives response from the server.
     *
     * @return a list of Strings as response in case the server returned
     * status OK, otherwise an exception is thrown.
     * @throws IOException
     */
    public List<String> finish() throws IOException {
        List<String> response = new ArrayList<String>();

        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();

        // checks server's status code first
        int status = connection.getResponseCode();
        String serverResponseMessage = connection.getResponseMessage().toString();
        Log.e("Tag -> ", "server msg -> " + serverResponseMessage);
        Log.e("Tag -> ", "ststus = " + status + " ,  created = " + HttpURLConnection.HTTP_CREATED);
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line = null;
            StringBuilder s_buffer = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                s_buffer.append(line);
            }
            reader.close();
            response.add(s_buffer.toString());
            connection.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return response;
    }

    public List<String> finishNewWay() throws IOException {
        List<String> response = new ArrayList<String>();

        // checks server's status code first
        int status = connection.getResponseCode();
        String serverResponseMessage = connection.getResponseMessage().toString();
        Log.e("Tag -> ", "server msg -> " + serverResponseMessage);
        Log.e("Tag -> ", "ststus = " + status + " ,  created = " + HttpURLConnection.HTTP_CREATED + " ,  ok = " + HttpURLConnection.HTTP_OK);
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(connection.getInputStream())));
            StringBuilder s_buffer = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                s_buffer.append(line);
            }
            response.add(s_buffer.toString());
            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
            connection.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }
        return response;
    }
}
