package com.wallpaperzilla.wallpaper.api;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wallpaperzilla.wallpaper.global.GlobalAppConfiguration;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.jacksonandroidnetworking.JacksonParserFactory;

import org.json.JSONObject;

import java.io.File;
import java.util.Locale;
import java.util.Properties;


public class APIServer {

    Context context;
    Properties properties;
    String subUrl = "";
    AppPrefrece appPrefrece;
    String mainUrl = "";

    public APIServer(Context context) {
        this.context = context;
        appPrefrece = new AppPrefrece(context);
        properties = new LoadAssetProperties().loadRESTApiFile(context.getResources(), "rest.properties", context);
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
    }

    public void logIn(final APIResponse listener, String is_fb, String social_id, String phone, String email, String name, String photo, String country,String device_id,
                      String time_zone, String lat, String lang, String city, String state, String one_signal_token) {

        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("logIn");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("is_fb", is_fb)
                .addBodyParameter("social_id", social_id)
                .addBodyParameter("phone", phone)
                .addBodyParameter("email", email)
                .addBodyParameter("name", name)
                .addBodyParameter("photo", photo)
                .addBodyParameter("country", country)
                .addBodyParameter("device_id", device_id)
                .addBodyParameter("device_name", Build.MANUFACTURER + " - " + Build.MODEL)
                .addBodyParameter("device_type", "Android")
                .addBodyParameter("time_zone", time_zone)
                .addBodyParameter("lat", lat)
                .addBodyParameter("long",lang)
                .addBodyParameter("city", city)
                .addBodyParameter("state", state)
                .addBodyParameter("one_signal_token",one_signal_token)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getHome(final APIResponse listener, String is_horizontal, String user_id, String returnIds, String imageIds) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getHome");

        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("returnIds", returnIds)
                .addBodyParameter("imageIds", imageIds)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getMessage());
                    }
                });
    }

    public void getImageByTag(final APIResponse listener, String is_horizontal, String user_id, String returnIds, String tag_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getImageByTag");

        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("user_id", user_id)
                .addQueryParameter("ids", returnIds)
                .addQueryParameter("tag_id", tag_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getMessage());
                    }
                });
    }

    public void imageLikeDislike(final APIResponse listener, String img_id, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("imageLikeDislike");

        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("img_id", img_id)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getTrendingImage(final APIResponse listener, String ids, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getTrendingImage");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("ids", ids)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getTrendingLive(final APIResponse listener, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getTrendingLive");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getLiveImage(final APIResponse listener, String ids, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getLiveImage");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("ids", ids)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void imageDownload(final APIResponse listener, String img_id, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("imageDownload");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("img_id", img_id)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void imageFavDisFav(final APIResponse listener, String img_id, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("imageFavDisFav");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("img_id", img_id)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getSavedImageList(final APIResponse listener, String ids, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getSavedImageList");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("ids", ids)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getDownloadImageList(final APIResponse listener, String ids, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getDownloadImageList");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("ids", ids)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void changeUserScore(final APIResponse listener, String is_add, String score, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("changeUserScore");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("is_add", is_add)
                .addBodyParameter("score", score)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void sendFeedBack(final APIResponse listener, String user_id, String feedback, String email) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("sendFeedBack");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("email", email)
                .addBodyParameter("feedback", feedback)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getCategory(final APIResponse listener, String is_horizontal) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getCategory");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("is_horizontal", is_horizontal)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getPremiumImage(final APIResponse listener, String ids, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getPremiumImage");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("ids", ids)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getRandomImage(final APIResponse listener, String ids, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getRandomImage");

        // Log.e("Tag","url "+mainUrl+subUrl);

        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("ids", ids)
                .addBodyParameter("is_horizontal", is_horizontal)
                .addBodyParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void updateUserPic(final APIResponse listener, File file, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("updateUserPic");
        AndroidNetworking.upload(mainUrl + subUrl)
                .addMultipartFile("image", file)
                .addMultipartParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getMyUploadedImage(final APIResponse listener, String is_horizontal, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getMyUploadedImage");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getAllTag(final APIResponse listener) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getAllTag");
        AndroidNetworking.get(mainUrl + subUrl)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void addImage(final APIResponse listener, File file, String user_id, String tag_ids, String is_horizontal) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("addImage");
        AndroidNetworking.upload(mainUrl + subUrl)
                .addMultipartFile("image", file)
                .addMultipartParameter("user_id", user_id)
                .addMultipartParameter("tag_ids", tag_ids)
                .addMultipartParameter("is_horizontal", is_horizontal)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void insertSingleTag(final APIResponse listener, String tag) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("insertSingleTag");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("tag", tag)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void deviceInfo(final APIResponse listener, String udid) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("deviceInfo");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("device_type", "Android")
                .addBodyParameter("datetime", Utility.getCurrentDateTimeServerFormate())
                .addBodyParameter("udid", udid)
                .addBodyParameter("device_model", Build.MANUFACTURER + " - " + Build.MODEL)
                .addBodyParameter("ios_version", android.os.Build.VERSION.SDK)
                .addBodyParameter("user_id", appPrefrece.getUserId())
                .addBodyParameter("country", new Locale("", tManager.getNetworkCountryIso()).getDisplayCountry())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void adSeenForAdFree(final APIResponse listener) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("adSeenForAdFree");
        AndroidNetworking.post(mainUrl + subUrl)
                .addBodyParameter("user_id", appPrefrece.getUserId())
                .addBodyParameter("udid", Utility.getDeviceId(context))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void imageSearchByTags(final APIResponse listener, String ids, String is_horizontal, String tag_ids, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("imageSearchByTags");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("ids", ids)
                .addQueryParameter("tag_ids", tag_ids)
                .addQueryParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void imageSearchByWord(final APIResponse listener, String ids, String is_horizontal, String words, String user_id) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("imageSearchByWord");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("is_horizontal", is_horizontal)
                .addQueryParameter("ids", ids)
                .addQueryParameter("words", words)
                .addQueryParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getTagForSearch(final APIResponse listener, String sort_by, String is_horizontal) {
        mainUrl = GlobalAppConfiguration.Main_Url;
        subUrl = properties.getProperty("getTagForSearch");
        AndroidNetworking.get(mainUrl + subUrl)
                .addQueryParameter("sort_by", sort_by)
                .addQueryParameter("is_horizontal", is_horizontal)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }

    public void getLocation(final APIResponse listener) {
        subUrl = properties.getProperty("location");
        AndroidNetworking.get(subUrl)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                        Log.e("Tag","Location : "+object.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getMessage());
                    }
                });
    }


}
