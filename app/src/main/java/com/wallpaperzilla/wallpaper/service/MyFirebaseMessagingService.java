package com.wallpaperzilla.wallpaper.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.SplashActivity;
import com.wallpaperzilla.wallpaper.notification.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Assure on 06-08-2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Tag -> ";

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From : " + remoteMessage.getFrom());
        Log.e(TAG, "isAppIsInBackground : " + NotificationUtils.isAppIsInBackground(getApplicationContext()));
        if (remoteMessage == null)
            return;
        // Check if message contains a notification payload.
        /*if (remoteMessage.getData() != null) {
            Log.e(TAG, "Data : " + remoteMessage.getData());

            if (remoteMessage.getData().size() > 0) {
                try {
                    JSONObject json = new JSONObject(remoteMessage.getData().toString());
                    Log.e(TAG, "Data Payload json : " + remoteMessage.getData().toString());
                    handleNotification(json);
                } catch (Exception e) {
                    Log.e(TAG, "Exception : " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Exception size ");
            }
        }*/

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body : " + remoteMessage.getNotification().getBody());
            if (remoteMessage.getData().size() > 0) {
                try {
                    JSONObject json = new JSONObject(remoteMessage.getData());
                    Log.e(TAG, "Data Payload json : " + remoteMessage.getData());
                    handleNotification(remoteMessage.getNotification().getTitle(), json);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }

    }

    private void handleNotification(String message, JSONObject json) {

        Log.e("Tag -> ", "is in back = " + NotificationUtils.isAppIsInBackground(getApplicationContext()));
        Intent resultIntent = null; //   default
        String title = getBaseContext().getResources().getString(R.string.app_name);
        resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("message", message);
        /*try {
            resultIntent.putExtra("notification_type", json.getString("notification_type"));
            resultIntent.putExtra("trip_id", json.getString("trip_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());


        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        getApplicationContext(),
                        0,
                        resultIntent,
                        PendingIntent.FLAG_ONE_SHOT
                );

        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/raw/notification");
        Notification notification = mBuilder
                .setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setSmallIcon(R.drawable.icon)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.icon))
                .setContentText(message)
                .build();
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(m, notification);
    }


    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
        notificationUtils.playNotificationSound();
    }

}
