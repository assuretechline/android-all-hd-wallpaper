package com.wallpaperzilla.wallpaper.sharedPrefrence;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by acer on 30-06-2017.
 */
public class AppPrefrece {
    SharedPreferences.Editor edt;
    String AppPrefrece = "AppPrefrece";
    String UserData = "UserData";
    String ISLOGIN = "isLogin";
    String ISBLACK = "isBlack";
    String ADSFREE = "adsFree";
    String TIMEFORFREE = "timeForFree";
    String ADSLINK = "adsLink";
    String ADSIMAGE = "adsImage";
    String ADSLINK2 = "adsLink2";
    String ADSIMAGE2 = "adsImage2";
    String ENDTIMEFORINTERVAL = "endTimeForInerval";
    String ENDTIMEFORFREE = "endTimeForFree";
    String ADINVERVAL = "adsInterval";
    String TIMEFORINTERVAL = "timeForInterval";
    String USER_ID = "user_id";
    String ACCESS_TOKEN = "access_token";
    String IsHorizontal = "0";
    String IS_FB = "is_fb";
    String SOCIAL_ID = "social_id";
    String PHONE = "phone";
    String EMAIL = "email";
    String NAME = "name";
    String PHOTO = "photo";


    String APP_ID = "app_id";
    String BANNER_ID = "banner_id";
    String INTESTRIAL_ID = "intestrial_id";
    String FB_INTESTRIAL_ID = "fb_intestrial_id";
    String MAIN_URL = "main_url";
    String REWARD_ID = "reward_id";
    String FORCE_UPDATE = "force_update";

    SharedPreferences sp;

    public AppPrefrece(Context context) {
        sp = context.getSharedPreferences(AppPrefrece, Context.MODE_PRIVATE);
        edt = sp.edit();
    }

    public void remove() {
        edt.remove(ISLOGIN);
        edt.remove(UserData);
        edt.remove(USER_ID);
        edt.remove(ACCESS_TOKEN);
        edt.remove(IsHorizontal);
        edt.remove(IS_FB);
        edt.remove(SOCIAL_ID);
        edt.remove(PHONE);
        edt.remove(EMAIL);
        edt.remove(NAME);
        edt.remove(PHOTO);
        edt.commit();
    }

    public JSONObject getProfile() throws JSONException {
        String strJson = sp.getString(UserData, "0");//second parameter is necessary ie.,Value to return if this preference does not exist.
        JSONObject jsonData = new JSONObject(strJson);
        return jsonData;
    }

    public void setProfile(JSONObject profile) {
        edt.putString(UserData, profile.toString());
        edt.commit();
    }

    public String getUserId() {
        return sp.getString(USER_ID, "0");
    }

    public void setUserId(String user_id) {
        edt.putString(USER_ID, user_id);
        edt.commit();
    }

    public String getIsHorizontal() {
        return sp.getString(IsHorizontal, "0");
    }

    public void setIsHorizontal(String isHorizontal) {
        edt.putString(IsHorizontal, isHorizontal);
        edt.commit();
    }

    public Boolean getLogin() {
        return sp.getBoolean(ISLOGIN, false);
    }

    public void setLogin(Boolean login) {
        edt.putBoolean(ISLOGIN, login);
        edt.commit();
    }

    public Boolean getBlack() {
        return sp.getBoolean(ISBLACK, true);
    }

    public void setBlack(Boolean isBlack) {
        edt.putBoolean(ISBLACK, isBlack);
        edt.commit();
    }

    public String getAPP_ID() {
        return sp.getString(APP_ID, "");
    }

    public void setAPP_ID(String app_id) {
        edt.putString(APP_ID, app_id);
        edt.commit();
    }

    public String getBANNER_ID() {
        return sp.getString(BANNER_ID, "");
    }

    public void setBANNER_ID(String banner_id) {
        edt.putString(BANNER_ID, banner_id);
        edt.commit();
    }

    public String getINTESTRIAL_ID() {
        return sp.getString(INTESTRIAL_ID, "");
    }

    public void setINTESTRIAL_ID(String intestrial_id) {
        edt.putString(INTESTRIAL_ID, intestrial_id);
        edt.commit();
    }

    public String getFB_INTESTRIAL_ID() {
        return sp.getString(FB_INTESTRIAL_ID, "");
    }

    public void setFB_INTESTRIAL_ID(String fb_intestrial_id) {
        edt.putString(FB_INTESTRIAL_ID, fb_intestrial_id);
        edt.commit();
    }

    public String getMAIN_URL() {
        return sp.getString(MAIN_URL, "");
    }

    public void setMAIN_URL(String main_url) {
        edt.putString(MAIN_URL, main_url);
        edt.commit();
    }

    public boolean getFORCE_UPDATE() {
        return sp.getBoolean(FORCE_UPDATE, false);
    }

    public void setFORCE_UPDATE(boolean force_update) {
        edt.putBoolean(FORCE_UPDATE, force_update);
        edt.commit();
    }

    public String getREWARD_ID() {
        return sp.getString(REWARD_ID, "");
    }

    public void setREWARD_ID(String reward_id) {
        edt.putString(REWARD_ID, reward_id);
        edt.commit();
    }


    public Boolean getAddFree() {
        return sp.getBoolean(ADSFREE, false);
    }

    public void setAddFree(Boolean addFree) {
        edt.putBoolean(ADSFREE, addFree);
        edt.commit();
    }

    public long getTimeForFree() {
        return sp.getLong(TIMEFORFREE, 0);
    }

    public void setTimeForFree(long addFree) {
        edt.putLong(TIMEFORFREE, addFree);
        edt.commit();
    }

    public String getEndTimeForFree() {
        return sp.getString(ENDTIMEFORFREE, "");
    }

    public void setEndTimeForFree(String addFree) {
        edt.putString(ENDTIMEFORFREE, addFree);
        edt.commit();
    }

    public Boolean getAddInterval() {
        return sp.getBoolean(ADINVERVAL, false);
    }

    public void setAddInterval(Boolean addFree) {
        edt.putBoolean(ADINVERVAL, addFree);
        edt.commit();
    }

    public long getTimeForInterval() {
        return sp.getLong(TIMEFORINTERVAL, 0);
    }

    public void setTimeForInterval(long addFree) {
        edt.putLong(TIMEFORINTERVAL, addFree);
        edt.commit();
    }

    public String getEndTimeForInterval() {
        return sp.getString(ENDTIMEFORINTERVAL, "");
    }

    public void setEndTimeForInterval(String addFree) {
        edt.putString(ENDTIMEFORINTERVAL, addFree);
        edt.commit();
    }

    public String getAdsLink() {
        return sp.getString(ADSLINK, "");
    }

    public void setAdsLink(String addFree) {
        edt.putString(ADSLINK, addFree);
        edt.commit();
    }

    public String getAdsImage() {
        return sp.getString(ADSIMAGE, "");
    }

    public void setAdsImage(String addFree) {
        edt.putString(ADSIMAGE, addFree);
        edt.commit();
    }

    public String getAdsLink2() {
        return sp.getString(ADSLINK2, "");
    }

    public void setAdsLink2(String addFree) {
        edt.putString(ADSLINK2, addFree);
        edt.commit();
    }

    public String getAdsImage2() {
        return sp.getString(ADSIMAGE2, "");
    }

    public void setAdsImage2(String addFree) {
        edt.putString(ADSIMAGE2, addFree);
        edt.commit();
    }

    public String getIS_FB() {
        return sp.getString(IS_FB, "");
    }

    public void setIS_FB(String _IS_FB) {
        edt.putString(IS_FB, _IS_FB);
        edt.commit();
    }

    public String getSOCIAL_ID() {
        return sp.getString(SOCIAL_ID, "");
    }

    public void setSOCIAL_ID(String _SOCIAL_ID) {
        edt.putString(SOCIAL_ID, _SOCIAL_ID);
        edt.commit();
    }

    public String getPHONE() {
        return sp.getString(PHONE, "");
    }

    public void setPHONE(String _PHONE) {
        edt.putString(PHONE, _PHONE);
        edt.commit();
    }

    public String getEMAIL() {
        return sp.getString(EMAIL, "");
    }

    public void setEMAIL(String _EMAIL) {
        edt.putString(EMAIL, _EMAIL);
        edt.commit();
    }

    public String getNAME() {
        return sp.getString(NAME, "");
    }

    public void setNAME(String _NAME) {
        edt.putString(NAME, _NAME);
        edt.commit();
    }

    public String getPHOTO() {
        return sp.getString(PHOTO, "");
    }

    public void setPHOTO(String _PHOTO) {
        edt.putString(PHOTO, _PHOTO);
        edt.commit();
    }


}
