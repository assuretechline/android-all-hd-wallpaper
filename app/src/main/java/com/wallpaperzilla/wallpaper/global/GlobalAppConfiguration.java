package com.wallpaperzilla.wallpaper.global;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.MainActivity;
import com.wallpaperzilla.wallpaper.model.MenuData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.ArrayList;

import static com.facebook.ads.AdSettings.IntegrationErrorMode.INTEGRATION_ERROR_CALLBACK_MODE;
import static com.facebook.ads.AdSettings.IntegrationErrorMode.INTEGRATION_ERROR_CRASH_DEBUG_MODE;

/**
 * Created by Assure on 15-03-2018.
 */

public class GlobalAppConfiguration extends MultiDexApplication {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";
    public final static int REQUEST_STORAGE = 101;
    public final static int RESULT_TAKE = 102;
    public final static int RESULT_GALLARY = 103;
    public static final int REQUEST_PERMISSION_STORAGE = 1005;
    public static String auth = "simplerestapi", client = "frontend-client";
    public static ArrayList<MenuData> menuDataList = new ArrayList<>();
    public static AppPrefrece appPrefrece;
    private static GlobalAppConfiguration appInstance;
    public static boolean fbAds=true;
    public static  String Main_Url = "";
    Context context;

    public static synchronized GlobalAppConfiguration getInstance() {
        return appInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appInstance = this;
        appPrefrece = new AppPrefrece(context);
        AudienceNetworkAds.initialize(this);
        AdSettings.setIntegrationErrorMode(INTEGRATION_ERROR_CALLBACK_MODE);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);

        menuDataList.add(new MenuData(getString(R.string.Home), false));
        menuDataList.add(new MenuData(getString(R.string.Search), false));
        menuDataList.add(new MenuData(getString(R.string.Saved), false));
        menuDataList.add(new MenuData(getString(R.string.Downloads), false));
        menuDataList.add(new MenuData(getString(R.string.upload_image), false));
        menuDataList.add(new MenuData(getString(R.string.earn_coins), false));
        menuDataList.add(new MenuData(getString(R.string.ad_free_trial), false));
        menuDataList.add(new MenuData(getString(R.string.ContactUs), false));
        menuDataList.add(new MenuData(getString(R.string.Feedback), false));
        menuDataList.add(new MenuData(getString(R.string.ShareAndRateus), false));
    }

    public static void startTimer() {
        new CountDownTimer(appPrefrece.getTimeForFree(), 1000) {
            public void onTick(long millisUntilFinished) {
                appPrefrece.setTimeForFree(millisUntilFinished);
            }

            public void onFinish() {
                if (MainActivity.getInstance().homeFragment.premiumFragment != null)
                    MainActivity.getInstance().homeFragment.premiumFragment.setData();
                appPrefrece.setTimeForFree(0);
                appPrefrece.setAddFree(false);
            }
        }.start();
    }

    public static void startTimerForInterval() {
        new CountDownTimer(appPrefrece.getTimeForInterval(), 1000) {
            public void onTick(long millisUntilFinished) {
                appPrefrece.setTimeForInterval(millisUntilFinished);
            }

            public void onFinish() {
                appPrefrece.setTimeForInterval(0);
                appPrefrece.setAddInterval(false);
            }
        }.start();
    }
}
