package com.wallpaperzilla.wallpaper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by home on 07/07/18.
 */

public class TempRecycleAdapter extends RecyclerView.Adapter<TempRecycleAdapter.Holder> {

    public ArrayList<MenuData> list;
    Context context;
    ImageLoader imageLoader;

    public TempRecycleAdapter(Context context, ArrayList<MenuData> list) {
        this.list = list;
        this.context = context;
        initialize();
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_menu, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        setData(holder, position);
        setColor(holder);
    }

    public void setData(Holder holder, final int position) {
        //imageLoader.displayImage(list.get(position).getProp_img(), holder.imgv_prop, Utility.getImageOptions());
        holder.txt_title.setText(list.get(position).getTitle());
    }

    public void setColor(Holder holder) {
        //holder.imgv_prop.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView txt_title;


        public Holder(View view) {
            super(view);
            txt_title = view.findViewById(R.id.txt_title);

        }
    }
}