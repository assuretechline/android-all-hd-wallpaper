package com.wallpaperzilla.wallpaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.SelectTagsActivity;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.TagSelectionData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class TagSelectionAdapter extends BaseAdapter implements Filterable {
    ImageLoader imageLoader;
    Context context;
    ArrayList<TagSelectionData> list;
    ArrayList<TagSelectionData> listTemp;
    ValueFilter valueFilter = new ValueFilter();

    public TagSelectionAdapter(Context context, ArrayList<TagSelectionData> list) {
        this.context = context;
        this.list = list;
        this.listTemp = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_tags_selection, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_value = view.findViewById(R.id.txt_value);
        holder.imgv_chbx = view.findViewById(R.id.imgv_chbx);
        holder.ll_main = view.findViewById(R.id.ll_main);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, final int position) {
        setPadding(holder, position);
        setFontStyle(holder);
        holder.txt_value.setText(list.get(position).getTag());

        if (list.get(position).isSelected()) {
            holder.imgv_chbx.setImageResource(R.drawable.check);
        } else {
            holder.imgv_chbx.setImageResource(R.drawable.uncheck);
        }

        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(position).isSelected()) {
                    list.get(position).setSelected(false);
                    SelectTagsActivity.tagdIds = SelectTagsActivity.tagdIds.replace("," + list.get(position).getTag_id() + ",", "");
                } else {
                    if (SelectTagsActivity.tagdIds.split(",").length != 6) {
                        list.get(position).setSelected(true);
                        SelectTagsActivity.tagdIds += "," + list.get(position).getTag_id() + ",";

                        TagSelectionData tag = list.get(position);
                        list.remove(position);
                        list.add(0, tag);


                    } else {
                        Utility.errDialog("You can select only 3 tags.", context);
                    }

                }
                notifyDataSetChanged();
            }
        });

    }

    private void setPadding(Holder holder, int position) {

    }

    private void setFontStyle(Holder holder) {

    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public Filter getFilter() {
        return valueFilter;
    }

    private static class Holder {
        TextView txt_value;
        ImageView imgv_chbx;
        LinearLayout ll_main;

    }

    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<TagSelectionData> filterList = new ArrayList<TagSelectionData>();
                for (int i = 0; i < listTemp.size(); i++) {
                    if (listTemp.get(i).getTag().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        filterList.add(listTemp.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = listTemp.size();
                results.values = listTemp;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            list = (ArrayList<TagSelectionData>) results.values;
            notifyDataSetChanged();
        }
    }
}
