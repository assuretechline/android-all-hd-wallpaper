package com.wallpaperzilla.wallpaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class SavedGridAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<ImageData> list;
    AppPrefrece appPrefrece;

    public SavedGridAdapter(Context context, ArrayList<ImageData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_saved_img, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.imgv_featured = view.findViewById(R.id.imgv_featured);
        holder.ll_main = view.findViewById(R.id.ll_main);
        //holder.mAdView = view.findViewById(R.id.adView);
        //setAdMobBannerAds(holder);
    }

    public void setAdMobBannerAds(Holder holder) {
        MobileAds.initialize(context, appPrefrece.getAPP_ID());
        holder.mAdView = new AdView(context);
        holder.mAdView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelOffset(R.dimen._135sdp)));
        holder.mAdView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        holder.mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
        holder.ll_main.addView(holder.mAdView);
    }

    @SuppressLint("NewApi")
    private void setData(final Holder holder, int position) {

        if (list.get(position).isContainAds()) {
            /*holder.imgv_featured.setVisibility(View.GONE);
            holder.mAdView.setVisibility(View.VISIBLE);

            if (!list.get(position).isAdsLoded()) {
                list.get(position).setAdsLoded(true);
                Thread adThread = new Thread() {
                    @Override
                    public void run() {
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    AdRequest adRequest = new AdRequest.Builder().build();
                                    holder.mAdView.loadAd(adRequest);
                                }catch (Exception e){
                                }

                            }
                        });
                    }
                };
                adThread.start();
                notifyDataSetChanged();
            }*/

            holder.imgv_featured.setVisibility(View.VISIBLE);
            if (position % 20 == 0)
                imageLoader.displayImage(appPrefrece.getAdsImage2(), holder.imgv_featured, Utility.getImageOptions());
            else
                imageLoader.displayImage(appPrefrece.getAdsImage(), holder.imgv_featured, Utility.getImageOptions());

        } else {
            //holder.mAdView.setVisibility(View.GONE);
            holder.imgv_featured.setVisibility(View.VISIBLE);
            imageLoader.displayImage(list.get(position).getSImage(), holder.imgv_featured, Utility.getImageOptions());
        }

    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        appPrefrece = new AppPrefrece(context);
    }

    private static class Holder {
        ImageView imgv_featured;
        LinearLayout ll_main;
        AdView mAdView;
    }
}
