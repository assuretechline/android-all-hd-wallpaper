package com.wallpaperzilla.wallpaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.CategoryData;
import com.google.zxing.common.StringUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class CategoryGridAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    ArrayList<CategoryData> list = new ArrayList<>();
    CategoryGridAdapter featuredGridAdapter;
    Context context;

    public CategoryGridAdapter(Context context, ArrayList<CategoryData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_category_img, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.imgv_featured = view.findViewById(R.id.imgv_featured);
        holder.ll_main = view.findViewById(R.id.ll_main);
        holder.txt_name = view.findViewById(R.id.txt_name);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        imageLoader.displayImage(list.get(position).getTag_photo(), holder.imgv_featured, Utility.getImageOptions());
        holder.txt_name.setText(Utility.titleWord(list.get(position).getTag()));
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private static class Holder {
        ImageView imgv_featured;
        TextView txt_name;
        LinearLayout ll_main;
    }
}
