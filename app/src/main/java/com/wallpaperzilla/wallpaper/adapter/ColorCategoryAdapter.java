package com.wallpaperzilla.wallpaper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.ViewImageActivity;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.CategoryData;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by home on 07/07/18.
 */

public class ColorCategoryAdapter extends RecyclerView.Adapter<ColorCategoryAdapter.Holder> {

    public ArrayList<CategoryData> list;
    Context context;
    ImageLoader imageLoader;

    public ColorCategoryAdapter(Context context, ArrayList<CategoryData> list) {
        this.list = list;
        this.context = context;
        initialize();
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_color_cat_img, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        setData(holder, position);
        setColor(holder);
    }

    public void setData(Holder holder, final int position) {
        imageLoader.displayImage(list.get(position).getTag_photo(), holder.cimgv_cat, Utility.getImageOptions());
        holder.cimgv_cat.setClickable(true);
        holder.cimgv_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewImageActivity.class);
                ImageData temp = new ImageData();
                temp.setContainAds(true);
                temp.setAdsLoded(false);
                i.putExtra("imageData", temp);
                i.putExtra("totalCount", list.get(position).getImage_total_count());
                i.putExtra("tag_id", list.get(position).getTag_id());
                context.startActivity(i);
                ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            }
        });
    }

    public void setColor(Holder holder) {
        //holder.imgv_prop.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        CircleImageView cimgv_cat;

        public Holder(View view) {
            super(view);
            cimgv_cat = view.findViewById(R.id.cimgv_cat);

        }
    }
}