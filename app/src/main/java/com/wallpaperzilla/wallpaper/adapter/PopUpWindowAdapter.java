package com.wallpaperzilla.wallpaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.model.PopWindowData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class PopUpWindowAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<PopWindowData> list;

    public PopUpWindowAdapter(Context context, ArrayList<PopWindowData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_popup_window, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_menu = view.findViewById(R.id.txt_menu);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        holder.txt_menu.setText(list.get(position).getTitle());

        if (list.get(position).isSelected()) {
            holder.txt_menu.setTypeface(Typeface.create("Sansation Bold", Typeface.BOLD));
        } else {
            holder.txt_menu.setTypeface(Typeface.create("Sansation Regular", Typeface.NORMAL));
        }
    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private static class Holder {
        TextView txt_menu;

    }
}
