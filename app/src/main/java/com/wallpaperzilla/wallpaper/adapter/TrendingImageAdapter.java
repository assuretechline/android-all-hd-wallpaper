package com.wallpaperzilla.wallpaper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.activity.LiveImageActivity;
import com.wallpaperzilla.wallpaper.activity.TrendingImageActivity;
import com.wallpaperzilla.wallpaper.global.Utility;
import com.wallpaperzilla.wallpaper.model.ImageData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by home on 07/07/18.
 */

public class TrendingImageAdapter extends RecyclerView.Adapter<TrendingImageAdapter.Holder> {

    public ArrayList<ImageData> list;
    Context context;
    ImageLoader imageLoader;
    AppPrefrece appPrefrece;

    public TrendingImageAdapter(Context context, ArrayList<ImageData> list) {
        this.list = list;
        this.context = context;
        initialize();
        appPrefrece = new AppPrefrece(context);
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_trending_img, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        setData(holder, position);
        setColor(holder);
    }

    public void setData(final Holder holder, final int position) {

        imageLoader.displayImage(list.get(position).getSImage(), holder.imgv_trending, Utility.getImageOptions());


       /* if (list.get(position).isContainAds()) {
            holder.imgv_trending.setVisibility(View.GONE);
            holder.mAdView.setVisibility(View.VISIBLE);
            if (!list.get(position).isAdsLoded()) {
                list.get(position).setAdsLoded(true);
                Thread adThread = new Thread() {
                    @Override
                    public void run() {
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    AdRequest adRequest = new AdRequest.Builder().build();
                                    holder.mAdView.loadAd(adRequest);
                                } catch (Exception e) {
                                    Log.e("Tag -> ", "Exception -> " + e.getMessage());
                                }

                            }
                        });
                    }
                };
                adThread.start();
               notifyDataSetChanged();
            }

        } else {
            holder.mAdView.setVisibility(View.GONE);
            holder.imgv_trending.setVisibility(View.VISIBLE);
            imageLoader.displayImage(list.get(position).getSImage(), holder.imgv_trending, Utility.getImageOptions());
        }*/

        holder.imgv_trending.setClickable(true);
        holder.imgv_trending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                if(list.get(position).getSImage().contains(".gif")){
                    i = new Intent(context, LiveImageActivity.class);
                }else{
                    i = new Intent(context, TrendingImageActivity.class);
                }
                i.putExtra("position", position);
                context.startActivity(i);
                ((Activity) context).overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            }
        });
    }

    public void setColor(Holder holder) {
        //holder.imgv_prop.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        RoundedImageView imgv_trending;
        LinearLayout ll_main;
//        AdView mAdView;

        public Holder(View view) {
            super(view);
            imgv_trending = view.findViewById(R.id.imgv_trending);
            ll_main = view.findViewById(R.id.ll_main);

//            MobileAds.initialize(context, appPrefrece.getAPP_ID());
//            mAdView = new AdView(context);
//            mAdView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//            mAdView.setAdSize(AdSize.MEDIUM_RECTANGLE);
//            mAdView.setAdUnitId(appPrefrece.getBANNER_ID());
//            ll_main.addView(mAdView);

        }
    }
}