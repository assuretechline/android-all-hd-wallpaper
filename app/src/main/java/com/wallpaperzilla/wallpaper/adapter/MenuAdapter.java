package com.wallpaperzilla.wallpaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wallpaperzilla.wallpaper.R;
import com.wallpaperzilla.wallpaper.interfac.Click;
import com.wallpaperzilla.wallpaper.model.MenuData;
import com.wallpaperzilla.wallpaper.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.suke.widget.SwitchButton;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class MenuAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<MenuData> list;
    AppPrefrece appPrefrece;

    public MenuAdapter(Context context, ArrayList<MenuData> list) {
        this.context = context;
        this.list = list;
        appPrefrece = new AppPrefrece(context);
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_menu, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
        holder.txt_circle = view.findViewById(R.id.txt_circle);
        holder.view_1 = view.findViewById(R.id.view_1);
        holder.view_2 = view.findViewById(R.id.view_2);
        holder.view_3 = view.findViewById(R.id.view_3);
    }

    @SuppressLint("NewApi")
    private void setData(final Holder holder, int position) {

        if (list.get(position).isSelecetd()) {
            holder.txt_circle.setVisibility(View.VISIBLE);
            holder.txt_title.setTypeface(Typeface.create("Sansation Bold", Typeface.BOLD));
        } else {
            holder.txt_circle.setVisibility(View.INVISIBLE);
            holder.txt_title.setTypeface(Typeface.create("Sansation Regular", Typeface.NORMAL));
        }

        if (list.get(position).getTitle().contains(context.getResources().getString(R.string.earn_coins))) {
            try {
                holder.txt_title.setText(list.get(position).getTitle() + " [ " + appPrefrece.getProfile().getString("user_score") + " ]");
            } catch (JSONException e) {
                holder.txt_title.setText(list.get(position).getTitle());
                e.printStackTrace();
            }
            holder.txt_title.setTextColor(context.getResources().getColor(R.color.yellow_text));
            holder.txt_title.setTypeface(Typeface.create("Sansation Bold", Typeface.BOLD));
        } else {
            holder.txt_title.setText(list.get(position).getTitle());
            holder.txt_title.setTextColor(context.getResources().getColor(R.color.black_font));
        }

        if (position == 0) {
            holder.view_1.setVisibility(View.INVISIBLE);
        } else {
            holder.view_1.setVisibility(View.VISIBLE);
        }

        if (position == list.size() - 1) {
            holder.view_2.setVisibility(View.INVISIBLE);
        } else {
            holder.view_2.setVisibility(View.VISIBLE);
        }

        holder.view_1.setBackgroundColor(context.getResources().getColor(R.color.black_background));
        holder.view_2.setBackgroundColor(context.getResources().getColor(R.color.black_background));
        holder.view_3.setBackground(context.getResources().getDrawable(R.drawable.circle_black));

    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private class Holder {
        TextView txt_title, txt_circle;
        View view_1, view_2, view_3;
    }
}
